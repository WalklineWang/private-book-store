<?php
include "../function/function.php";
session_start();

if (!isset($_SESSION["uid"])) {
	header ("location: ../");
	exit();
}

if (!isset($_GET["catalog"]) || empty($_GET["catalog"]) || !isset($_GET["title"]) || empty($_GET["title"])) {
	header("location: ./");
	exit();
}

$catalog = $_GET["catalog"];

if ($catalog === "an-ye-jiang-zhi") {
	$title = $_GET["title"];

	$url = "http://www.anyejiangzhi.com/";

	if (!$homepage = getUrlData($url)) {
		header ("location: ../error.php?fromurl=" . urlencode($_SERVER['PHP_SELF'] . '?' . $_SERVER['QUERY_STRING']));
	}

	$output = file_get_contents("./interface/home.interface");

	//取目录列表
	preg_match_all("/<ul class=\"list clearfix\">[\s\S]*?<\/ul>/", $homepage, $cataloglists);

	$lists = "";
	foreach($cataloglists[0] as $list )
	{
		$lists .= "<li>" . str_replace("\n", "", rtrim(ltrim(strip_tags($list, "<li><a>")))) . "</li>\n";
	}

	$output = str_replace("###TITLE###", $title, $output);
	$output = str_replace("###CATALOGLISTS###", $lists, $output);

	$output = str_replace("http://www.anyejiangzhi.com/", "./article.php?catalog={$catalog}&title={$title}&page=", $output);	
} else if($catalog === "mian-zhuan") {
	$title = $_GET["title"];

	$url = "http://www.mdjywl.com/";

	if (!$homepage = getUrlData($url)) {
		header ("location: ../error.php?fromurl=" . urlencode($_SERVER['PHP_SELF'] . '?' . $_SERVER['QUERY_STRING']));
	}

	$output = file_get_contents("./interface/home.interface");

	//取目录列表
	preg_match_all("/<li><a href=\"http:\/\/www.mdjywl.com\/min-diao-ju-yi-wen-lu-mian-zhuan-[\s\S]*?<\/ul>/", $homepage, $cataloglists);

	$lists = "";
	foreach($cataloglists[0] as $list )
	{
		$lists .= "<li>" . str_replace("\n", "", rtrim(ltrim(strip_tags($list, "<li><a>")))) . "</li>\n";
	}

	$output = str_replace("###TITLE###", $title, $output);
	$output = str_replace("###CATALOGLISTS###", $lists, $output);

	$output = str_replace("http://www.mdjywl.com/min-diao-ju-yi-wen-lu-mian-zhuan-", "./article.php?catalog={$catalog}&title={$title}&page=", $output);
} else if($catalog === "dong-han-feng-yun") {
	$title = $_GET["title"];

	$url = "http://www.mdjywl.com/";

	if (!$homepage = getUrlData($url)) {
		header ("location: ../error.php?fromurl=" . urlencode($_SERVER['PHP_SELF'] . '?' . $_SERVER['QUERY_STRING']));
	}

	$output = file_get_contents("./interface/home.interface");

	//取目录列表
	preg_match_all("/<li><a href=\"http:\/\/www.mdjywl.com\/min-diao-ju-yi-wen-lu-dong-han-feng-yun-[\s\S]*?<\/ul>/", $homepage, $cataloglists);

	$lists = "";
	foreach($cataloglists[0] as $list )
	{
		$lists .= "<li>" . str_replace("\n", "", rtrim(ltrim(strip_tags($list, "<li><a>")))) . "</li>\n";
	}

	$output = str_replace("###TITLE###", $title, $output);
	$output = str_replace("###CATALOGLISTS###", $lists, $output);

	$output = str_replace("http://www.mdjywl.com/min-diao-ju-yi-wen-lu-dong-han-feng-yun-", "./article.php?catalog={$catalog}&title={$title}&page=", $output);
} else {
	$title = $_GET["title"];
	$param = "book/min-diao-ju-yi-wen-lu-";
	$url = "http://www.mindiaojuyiwenlu.com/{$param}{$catalog}";

	if (!$homepage = getUrlData($url)) {
		header ("location: ../error.php?fromurl=" . urlencode($_SERVER['PHP_SELF'] . '?' . $_SERVER['QUERY_STRING']));
	}
	$output = file_get_contents("./interface/home.interface");

	//取目录列表
	preg_match_all("/<div class=\"entry-title[\s\S]*?<\/div>/", $homepage, $cataloglists);

	$lists = "";
	foreach($cataloglists[0] as $list )
	{
		$lists .= "<li>" . str_replace("\n", "", rtrim(ltrim(strip_tags($list, "<a>")))) . "</li>\n";
	}

	$output = str_replace("###TITLE###", $title, $output);
	$output = str_replace("###CATALOGLISTS###", $lists, $output);

	$output = str_replace("http://www.mindiaojuyiwenlu.com/", "./article.php?catalog={$catalog}&title={$title}&page=", $output);
}

echo $output;	
?>