<?php
include "../function/function.php";
include "../function/conn.php";
session_start();

if (!isset($_SESSION["uid"])) {
	header ("location: ../");
	exit();
}

if (!isset($_GET["catalog"]) || empty($_GET["catalog"]) || !isset($_GET["title"]) || empty($_GET["title"]) || !isset($_GET["page"]) || empty($_GET["page"])) {
	header ('location: ./');
}

$uid = $_SESSION["uid"];
$catalog = $_GET["catalog"];

if ($catalog === "an-ye-jiang-zhi") {
	$page = $_GET["page"];
	$title = $_GET["title"];
	$encodedtitle = urlencode($title);
	$url = "http://www.anyejiangzhi.com/{$page}";

	if (!$article = getUrlData($url)) {
		header ("location: ../error.php?fromurl=" . urlencode($_SERVER['PHP_SELF'] . '?' . $_SERVER['QUERY_STRING']));
		exit();
	}

	$output = file_get_contents("./interface/article.interface");

	//获取小说正文
	preg_match('/<div class=\"content\">[\s\S]*?<\/div>/', $article, $contents);
	$contents = strip_tags($contents[0], "<p>");
	$contents = str_replace("<p>", "<p>　　", $contents);

	//取章节名称
	preg_match_all("/<h1>.*?<\/h1>/", $article, $atitle);
	$atitle = strip_tags($atitle[0][1]);

	//获取上一章节、下一章节链接
	$result = preg_match_all("/<a href=\"http:\/\/www.anyejiangzhi.com\/an-ye-jiang-zhi-[\s\S]*?<\/a>/", $article, $titles);

	$ptitle = $titles[0][0];
	$ntitle = "";
	if (count($titles[0]) > 1) {$ntitle = $titles[0][1];}

	$output = str_replace("###TITLE###", $title, $output);
	$output = str_replace("###ARTICLETITLE###", $atitle, $output);
	$output = str_replace("###CONTENTS###", $contents, $output);
	$output = str_replace("###PREVTITLE###", $ptitle, $output);
	$output = str_replace("###NEXTTITLE###", $ntitle, $output);
	$output = str_replace("###CATALOGLINK###", "./home.php?catalog={$catalog}&title={$encodedtitle}", $output);
	$output = str_replace("###CATALOG###", "<a href='./home.php?catalog={$catalog}&title={$encodedtitle}'>目录</a>", $output);
	$output = str_replace("http://www.anyejiangzhi.com/", "./article.php?catalog={$catalog}&title={$encodedtitle}&page=", $output);
} else if ($catalog === "mian-zhuan") {
	$page = $_GET["page"];
	$title = $_GET["title"];
	$encodedtitle = urlencode($title);
	$url = "http://www.mdjywl.com/min-diao-ju-yi-wen-lu-mian-zhuan-{$page}";

	if (!$article = getUrlData($url)) {
		header ("location: ../error.php?fromurl=" . urlencode($_SERVER['PHP_SELF'] . '?' . $_SERVER['QUERY_STRING']));
		exit();
	}

	$output = file_get_contents("./interface/article.interface");

	//获取小说正文
	preg_match('/<div class=\"content\">[\s\S]*?<\/div>/', $article, $contents);
	$contents = strip_tags($contents[0], "<p>");
	$contents = str_replace("<p>", "<p>　　", $contents);

	//取章节名称
	preg_match_all("/<h1>.*?<\/h1>/", $article, $atitle);
	$atitle = strip_tags($atitle[0][1]);

	//获取上一章节、下一章节链接
	$result = preg_match_all("/<a href=\"http:\/\/www.mdjywl.com\/min-diao-ju-yi-wen-lu-mian-zhuan-[\s\S]*?<\/a>/", $article, $titles);

	$ptitle = $titles[0][0];
	$ntitle = "";
	if (count($titles[0]) > 1) {$ntitle = $titles[0][1];}

	$output = str_replace("###TITLE###", $title, $output);
	$output = str_replace("###ARTICLETITLE###", $atitle, $output);
	$output = str_replace("###CONTENTS###", $contents, $output);
	$output = str_replace("###PREVTITLE###", $ptitle, $output);
	$output = str_replace("###NEXTTITLE###", $ntitle, $output);
	$output = str_replace("###CATALOGLINK###", "./home.php?catalog={$catalog}&title={$encodedtitle}", $output);
	$output = str_replace("###CATALOG###", "<a href='./home.php?catalog={$catalog}&title={$encodedtitle}'>目录</a>", $output);
	$output = str_replace("http://www.mdjywl.com/min-diao-ju-yi-wen-lu-mian-zhuan-", "./article.php?catalog={$catalog}&title={$encodedtitle}&page=", $output);
} else if ($catalog === "dong-han-feng-yun") {
	$page = $_GET["page"];
	$title = $_GET["title"];
	$encodedtitle = urlencode($title);
	$url = "http://www.mdjywl.com/min-diao-ju-yi-wen-lu-dong-han-feng-yun-{$page}";

	if (!$article = getUrlData($url)) {
		header ("location: ../error.php?fromurl=" . urlencode($_SERVER['PHP_SELF'] . '?' . $_SERVER['QUERY_STRING']));
		exit();
	}

	$output = file_get_contents("./interface/article.interface");

	//获取小说正文
	preg_match('/<div class=\"content\">[\s\S]*?<\/div>/', $article, $contents);
	$contents = strip_tags($contents[0], "<p>");
	$contents = str_replace("<p>", "<p>　　", $contents);

	//取章节名称
	preg_match_all("/<h1>.*?<\/h1>/", $article, $atitle);
	$atitle = strip_tags($atitle[0][1]);

	//获取上一章节、下一章节链接
	$result = preg_match_all("/<a href=\"http:\/\/www.mdjywl.com\/min-diao-ju-yi-wen-lu-dong-han-feng-yun-[\s\S]*?<\/a>/", $article, $titles);

	$ptitle = $titles[0][0];
	$ntitle = "";
	if (count($titles[0]) > 1) {$ntitle = $titles[0][1];}

	$output = str_replace("###TITLE###", $title, $output);
	$output = str_replace("###ARTICLETITLE###", $atitle, $output);
	$output = str_replace("###CONTENTS###", $contents, $output);
	$output = str_replace("###PREVTITLE###", $ptitle, $output);
	$output = str_replace("###NEXTTITLE###", $ntitle, $output);
	$output = str_replace("###CATALOGLINK###", "./home.php?catalog={$catalog}&title={$encodedtitle}", $output);
	$output = str_replace("###CATALOG###", "<a href='./home.php?catalog={$catalog}&title={$encodedtitle}'>目录</a>", $output);
	$output = str_replace("http://www.mdjywl.com/min-diao-ju-yi-wen-lu-dong-han-feng-yun-", "./article.php?catalog={$catalog}&title={$encodedtitle}&page=", $output);
} else {
	$page = $_GET["page"];
	$title = $_GET["title"];
	$encodedtitle = urlencode($title);
	$url = "http://www.mindiaojuyiwenlu.com/{$page}";

	if (!$article = getUrlData($url)) {
		header ("location: ../error.php?fromurl=" . urlencode($_SERVER['PHP_SELF'] . '?' . $_SERVER['QUERY_STRING']));
		exit();
	}

	$output = file_get_contents("./interface/article.interface");

	//获取小说正文
	preg_match('/<div class="single-box clearfix[\s\S]*?<\/div>/', $article, $contents);
	$contents = strip_tags($contents[0], "<p>");

	//取章节名称
	preg_match("/<h1 class=\"entry-title.*?<\/h1>/", $article, $atitle);
	$atitle = strip_tags($atitle[0]);

	//获取上一章节链接
	$result = preg_match("/<div class=\"nav-previous[\s\S]*?<\/div>/", $article, $ptitle);

	if (!$result || trim(strip_tags($ptitle[0])) == "") {
		$ptitle = "<a href='./home.php?catalog={$catalog}&title={$encodedtitle}'>上一篇</a>";
	} else {
		preg_match("/<\/span>.*?<\/a>/", $ptitle[0], $temp);
		$ptitle = rtrim(ltrim(strip_tags(str_replace($temp[0], "</span></a>", $ptitle[0]), "<a>")));
	}

	//获取下一章节链接
	$result = preg_match("/<div class=\"nav-next[\s\S]*?<\/div>/", $article, $ntitle);

	if (!$result || trim(strip_tags($ntitle[0])) == "") {
		$ntitle = "<a href='./home.php?catalog={$catalog}&title={$encodedtitle}'>下一篇</a>";
	} else {
		preg_match("/<\/span>.*?<\/a>/", $ntitle[0], $temp);
		$ntitle = rtrim(ltrim(strip_tags(str_replace($temp[0], "</span></a>", $ntitle[0]), "<a>")));
	}

	$output = str_replace("###TITLE###", $title, $output);
	$output = str_replace("###ARTICLETITLE###", $atitle, $output);
	$output = str_replace("###CONTENTS###", $contents, $output);
	$output = str_replace("###PREVTITLE###", $ptitle, $output);
	$output = str_replace("###NEXTTITLE###", $ntitle, $output);
	$output = str_replace("###CATALOGLINK###", "./home.php?catalog={$catalog}&title={$encodedtitle}", $output);
	$output = str_replace("###CATALOG###", "<a href='./home.php?catalog={$catalog}&title={$encodedtitle}'>目录</a>", $output);
	$output = str_replace("http://www.mindiaojuyiwenlu.com/", "./article.php?catalog={$catalog}&title={$encodedtitle}&page=", $output);
}

echo $output;

$result = mysql_query("select * from book_history where bid='mdj-$catalog' and uid='$uid' and bfrom='mdj' limit 1");

if (mysql_num_rows($result)) {
	mysql_query("update book_history set btitle='$atitle', burl='./article.php?catalog=$catalog&page=$page&title=$title' where bid='mdj-$catalog' and uid='$uid' and bfrom='mdj' limit 1");
} else {
	mysql_query("insert into book_history (bid, btitle, burl, bfrom, uid) values ('mdj-$catalog', '$atitle', './article.php?catalog=$catalog&page=$page&title=$title', 'mdj', '$uid')");
}

mysql_free_result($result);
mysql_close($conn);
?>