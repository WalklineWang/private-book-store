<?php
session_start();
include_once('config.php');
include_once('saetv2.ex.class.php');

$oauth = new SaeTOAuthV2(WB_AKEY, WB_SKEY);

if (isset($_REQUEST['code'])) {
	$keys = array();
	$keys['code'] = $_REQUEST['code'];
	$keys['redirect_uri'] = WB_CALLBACK_URL;

	try {
		$token = $oauth->getAccessToken('code', $keys);
	} catch (OAuthException $e) {print_r($e);}
}

if ($token) {
	$_SESSION['token'] = $token;
	setcookie('weibojs_'.$oauth->client_id, http_build_query($token));
	header ("location: ./success.php");
} else {
	header ("location: ./error.php");
}
?>