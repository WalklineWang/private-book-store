<?php
session_start();
include "../../function/conn.php";
include_once "./config.php";
include_once "./saetv2.ex.class.php";

if (!isset($_SESSION["token"]) || empty($_SESSION["token"])) {
	header ("location: ../../");
	exit();
}

$client = new SaeTClientV2(WB_AKEY, WB_SKEY, $_SESSION["token"]["access_token"]);

$uid_get = $client->get_uid();

$uid = $uid_get['uid'];
$user_info = $client->show_user_by_id($uid);
$screen_name = $user_info["screen_name"];
$location = $user_info["location"];
$gender = $user_info["gender"];
$followers_count = $user_info["followers_count"];
$avatar = $user_info["avatar_hd"];
$auth_date = date("Y-m-d H:i:s");

$_SESSION["uid"] = $uid;

$output = file_get_contents("./interface/success.interface");
$output = str_replace("###SCREENNAME###", $user_info['screen_name'], $output);

$result = mysql_query("select * from book_wbuser where uid='$uid' limit 1");

if (mysql_num_rows($result) > 0) {
	mysql_query("update book_wbuser set screen_name='$screen_name', location='$location', gender='$gender', followers_count='$followers_count', avatar='$avatar', auth_date='$auth_date' where uid='$uid' limit 1");
} else {
	mysql_query("insert into book_wbuser (uid, screen_name, location, gender, followers_count, avatar, auth_date) values ('$uid', '$screen_name', '$location', '$gender', '$followers_count', '$avatar', '$auth_date')");
}

echo $output;

mysql_free_result($result);
mysql_close($conn);
?>