var xmlHttp;
var arrows = {
	"none": "url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAQAAAAAYLlVAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAAmJLR0QAAKqNIzIAAAAJcEhZcwAABLAAAASwAJArFzAAAAKwSURBVGje7ZdNSFRRFMd/r5lJ06HCspKMnELpU4gyiCgqadMXRVJREYRgUy1KWkSBTBK1KdoUEu2iKCKSPjbaB1FEkQWluwyLUDODFLQZP8fbwsdp9D115L0ZN/e3mnfvued/5sy9//sGNBqNRqPRaCYYTxwxM1hBlM5xZs4iny4izkucTxVhPrJ5XKsK+UCYanKcF1CCQqFoZG9c/QIPRfwwVx1xXsAeesxk7ZQyecx4Hyf4Y67oYZ/zAvxcpddMGKYc/6jR6YT4a0b3cm2M6DhJ5wIRSXqdzBEjZ1Ih/Ypw0R15gBRO0mYmjnKfbNuoudwjGvNzpbglD+DhAD/N5IoX5FsilvJU5ls4GOeGHQcG2/giErWsHTK7hk8yV892DLflBymgRmS+iozBFuplvIbViREfZDHPROoXh/Hg4RAtMvacJYmUB8jmDv2y1U5RSrv51M9d5iVaHiCDCnGGbrrNT31UkJEMeQA/ZXRI2xWKDkLunXorXsuu9hIUw1W0cRTfsAgDrzviqRyjknPMHjY+if00oVA02Zz6WYSo5DhTnBewkzCKAR6z0DK3iQdUUmgZD/CQKIoIu5wXUCKNfmPjfz5L62E5r2VN0HkBeTEeV8vGMePX81ni61jkvABYxTtJ2UjRKDZrsFteRVz1xAU8YsBM+5sSm7YDeCmm1Ywa4Am5bskDzOGm+F8nZaRZItI4K97Qz22y3JQHmMYlusT/rjB9yOxULosnWmddIpXTdNr6fja36JP+nHHj9Nvjozjm5qsmD4BcqmSsleAIO8QlDLbSIHLvKWAlb+X5OzsS9SoSy7qYk94QU04dGxIvPsgyXg65CxWKVzY+mUByTLf/f+oDyZQHyJT/AL3csNyVScFPOc00c97Jq4izPesjgME3+ibi+2s0Go1Go3GJf+QWQl14E+2HAAAAJXRFWHRkYXRlOmNyZWF0ZQAyMDE1LTA3LTI1VDIxOjQ5OjI3KzA4OjAw56k66QAAACV0RVh0ZGF0ZTptb2RpZnkAMjAxNC0wNC0wNlQwOTozOTo1MyswODowMLOia0AAAABOdEVYdHNvZnR3YXJlAEltYWdlTWFnaWNrIDYuOC44LTEwIFExNiB4ODZfNjQgMjAxNS0wNy0xOSBodHRwOi8vd3d3LmltYWdlbWFnaWNrLm9yZwUMnDUAAAAjdEVYdHN2Zzpjb21tZW50ACBHZW5lcmF0b3I6IEljb01vb24uaW8gvDCsgAAAABh0RVh0VGh1bWI6OkRvY3VtZW50OjpQYWdlcwAxp/+7LwAAABh0RVh0VGh1bWI6OkltYWdlOjpIZWlnaHQANTMzyrwBlQAAABd0RVh0VGh1bWI6OkltYWdlOjpXaWR0aAA1MzNZTVHIAAAAGXRFWHRUaHVtYjo6TWltZXR5cGUAaW1hZ2UvcG5nP7JWTgAAABd0RVh0VGh1bWI6Ok1UaW1lADEzOTY3NDgzOTNTu/4AAAAAE3RFWHRUaHVtYjo6U2l6ZQA0LjkyS0JCgUAF7AAAAFp0RVh0VGh1bWI6OlVSSQBmaWxlOi8vL2hvbWUvd3d3cm9vdC93d3cuZWFzeWljb24ubmV0L2Nkbi1pbWcuZWFzeWljb24uY24vc3JjLzExNDMyLzExNDMyNjYucG5nyq/cqwAAAABJRU5ErkJggg==)",
	"block": "url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAQAAAAAYLlVAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAAmJLR0QAAKqNIzIAAAAJcEhZcwAABLAAAASwAJArFzAAAAKoSURBVGje7ZdLSFRRGMd/4/iIprTBcKboIVgIFRZpm0zIKCgTiggJamFFUVBghEGUlFSLXhgWtQiJkFpEFEYLC6KICmsRWTQ9TBcWWmaWlQ5jo7eFh49rM+rYvTOzOf+7mXvOd8/vP/c8vu+ClpaWlpaWVpzltPh8Om56MeJjPoG1PKWJA7jigU9iGx0YGAQ4R3qs8eM5yC8MdfVzg5mxxKdxmoDgB697zIkVfhpXCCrsT9rEwnPyY4GfTb0g29lCPs/k/gNF0cYvokFwLRTjALK5K21tlJIYPfwK3gqqkQJpn8E106TsY1w04E5KaBX8feYN6XVzRpaln+Ok2o1PpowuBRigjsyQCBeH+K0i/nAJj514F5X0qMH7OE9G2KgkdvJVTN4MY/I/NZkL9KmBeznGhGEjEyjhk0zTY3LtwKdSy4Aasos9oy6w5bwyLdQc6waK8Muu3xRR9pzPE7FwwrqBQroxMHhHcUhfJpVUsySkfRa31VursG4gmXLecIu8MP/0IQYGrZSQ8E+flyreU8NU6wbAiSdMxl9Mo7zoTraHnH9JeKNzIAE4WE3TkFzYzf7YlSVOSvks+71f/QpwFncs8CmU8V1Bg1zlMD/krtaeOR9JaZyi13QmunGylS8yFfVkRxOfQY0p61WoM9HBGprFQgMLo4XPok5mvJMdQ9Z9AS/Fgo9VOOzH5/BIEOF2/gIemMqSDSH9FrWUFzL8a5aFjZnOdcka39hNsl1wB+v5aJrjvGEjM7goebOHI/acDA42qw+QwVIka8ToiRyVyiFAtR2VUZ4U3kEu4x01PoW9ppNhl3UD69RL9XOSSRE9kchGKUuqrBvwcIcgHZSPKbWsxEeQFgpHC4xkv04hl3YaCY7JeBZzacYXr093LS0tLS0tLa2I9RcPRUP/lN/6hgAAACV0RVh0ZGF0ZTpjcmVhdGUAMjAxNS0wNy0yNVQyMTo0OToyNyswODowMOepOukAAAAldEVYdGRhdGU6bW9kaWZ5ADIwMTQtMDQtMDZUMDk6Mzk6NTkrMDg6MDAX0jQOAAAATnRFWHRzb2Z0d2FyZQBJbWFnZU1hZ2ljayA2LjguOC0xMCBRMTYgeDg2XzY0IDIwMTUtMDctMTkgaHR0cDovL3d3dy5pbWFnZW1hZ2ljay5vcmcFDJw1AAAAI3RFWHRzdmc6Y29tbWVudAAgR2VuZXJhdG9yOiBJY29Nb29uLmlvILwwrIAAAAAYdEVYdFRodW1iOjpEb2N1bWVudDo6UGFnZXMAMaf/uy8AAAAYdEVYdFRodW1iOjpJbWFnZTo6SGVpZ2h0ADUzM8q8AZUAAAAXdEVYdFRodW1iOjpJbWFnZTo6V2lkdGgANTMzWU1RyAAAABl0RVh0VGh1bWI6Ok1pbWV0eXBlAGltYWdlL3BuZz+yVk4AAAAXdEVYdFRodW1iOjpNVGltZQAxMzk2NzQ4Mzk5s24XHgAAABN0RVh0VGh1bWI6OlNpemUANS4wNktCQqVF+34AAABadEVYdFRodW1iOjpVUkkAZmlsZTovLy9ob21lL3d3d3Jvb3Qvd3d3LmVhc3lpY29uLm5ldC9jZG4taW1nLmVhc3lpY29uLmNuL3NyYy8xMTQzMi8xMTQzMjc1LnBuZ0ZTdd4AAAAASUVORK5CYII=)"
};

/**
* 初始化小说网站模块折叠状态
*
* 调用来自: index.interface, manage.interface
*/
function init() {
	var headers = new Array("zyg_header", "dr_header", "dd_header", "tt_header", "mf_header", "bl_header");
	var lists = new Array("zyg_lists", "dr_lists", "dd_lists", "tt_lists", "mf_lists", "80_lists");

	for (var count = 0; count < headers.length; count++)
	{
		var status = getCookie(headers[count] + "_status");

		if (status != null) {
			document.getElementById(lists[count]).style.display = status;
			document.querySelector("#" + headers[count] + ">span").style.backgroundImage = arrows[status];
		} else {
			document.getElementById(lists[count]).style.display = "block";
			document.querySelector("#" + headers[count] + ">span").style.backgroundImage = arrows["block"];
		}
	}

	document.getElementById("80_lists").style.display = "none";
	document.querySelector("#bl_header>span").style.backgroundImage = arrows["none"];
	document.getElementById("lq_lists").style.display = "none";
	document.querySelector("#lq_header>span").style.backgroundImage = arrows["none"];
	document.getElementById("zyg_lists").style.display = "none";
	document.querySelector("#zyg_header>span").style.backgroundImage = arrows["none"];
}

/**
* 将查询小说的返回信息写入写入对应栏目
* @param param json字符串
* 
* 调用来自: function.js->stateChanged()
*/
function fillInfo(param) {
	var obj = JSON.parse(param);

	document.getElementById('bfrom').value = obj.bfrom;
	document.getElementById('bid').value = obj.bid;
	document.getElementById('btitle').value = obj.btitle;
	document.getElementById('bauth').value = obj.bauth;
}

/**
* 获取当前访问URL的参数
*
* 调用来自: article.interface, function.js->saveLastPos()
*			, function.js->saveLastPos(), function.js->doAppend()
*			, function->doImport()
*/
function getArgs() {
    var args = {};
    var match = null;
    var search = decodeURIComponent(location.search.substring(1));
    var reg = /(?:([^&]+)=([^&]+))/g;

    while ((match = reg.exec(search)) !== null) {args[match[1]] = match[2];}

    return args;
}

/**
* 滚动到上次浏览的位置
*
* 位置信息存储于cookie中，由bid_page区分
* 调用来自: article.interface
*/
function scroll2LastPos() {
	var bid = getArgs()["bid"];

	if (bid == "undefined") {bid = getArgs()["title"];}

	var id = bid + "_" + getArgs()["page"];
	var body = document.body || document.documentElement;
	var pos = getCookie(id);

	if (pos != null) {body.scrollTop = pos;}
}

/**
* 记录页面浏览位置
*
* 位置信息存储于cookie中，由bid_page区分
* 调用来自: article.interface
*/
function saveLastPos() {
	var bid = getArgs()["bid"];

	if (bid == "undefined") {bid = getArgs()["title"];}

	var id = bid + "_" + getArgs()["page"];
	var pos = document.documentElement.scrollTop || document.body.scrollTop;
	
	setCookie(id + "=" + pos);
}

/**
* 从cookie中读取指定信息
*
* @param id 指定信息的id
* @return 指定信息的value
*/
function getCookie(id) {
	var arr = {};
	var reg = new RegExp("(^| )" + id + "=([^;]*)(;|$)");

	if (arr = document.cookie.match(reg)) {
		return unescape(arr[2]);
	} else {
		return null;
	}
}

/** 
* 保存cookie信息，并设置过期时间为30天
*
* @param string 需要保存的id和value的拼接字符串
*/
function setCookie(string) {
	if (string.length == 0) {return;}

	var date = new Date();
	var expireDay = 30;

	date.setDate(date.getDate() + expireDay);
	document.cookie = string + "; expires=" + date.toUTCString();
}

function showTips(status) {
	if (status === "success") {
		var tips = document.createElement("div");

		tips.id = "tips";
		tips.className = "success";
		tips.innerHTML = "<div id='tips-content'>操作成功</div>";

		document.body.appendChild(tips);

		countDown(3);
	} else if (status === "failed") {
		var tips = document.createElement("div");

		tips.id = "tips";
		tips.className = "failed";
		tips.innerHTML = "<div id='tips-content'>操作失败</div>";

		document.body.appendChild(tips);

		countDown(3);
	}
}

var time = 3;
function countDown() {
	if (time == 0) {
		document.body.removeChild(document.getElementById("tips"));
	}

	setTimeout("countDown()",1000);
	time -= 1;
}

//用于折叠index.interface和manage.interface中的小说网站模块
function hideList(header) {
	switch (header.id) {
		case "zyg_header":
			var oContent = document.getElementById("zyg_lists");
			break;
		case "dr_header":
			var oContent = document.getElementById("dr_lists");
			break;
		case "lq_header":
			var oContent = document.getElementById("lq_lists");
			break;
		case "dd_header":
			var oContent = document.getElementById("dd_lists");
			break;
		case "tt_header":
			var oContent = document.getElementById("tt_lists");
			break;
		case "mf_header":
			var oContent = document.getElementById("mf_lists");
			break;
		case "bl_header":
			var oContent = document.getElementById("80_lists");
	}

	if (oContent.style.display == "none") {
		oContent.style.display = "block";
		setCookie(header.id + "_status=block");
		document.querySelector("#"+header.id+">span").style.backgroundImage = arrows["block"];
	} else {
		oContent.style.display = "none";
		setCookie(header.id + "_status=none");
		document.querySelector("#"+header.id+">span").style.backgroundImage = arrows["none"];
	}
}

//index.interface版全信息中的年份
function getYear() {document.write(new Date().getFullYear());}

//manage.php中用于更新、删除小说时点击小说名称后将信息自动填写到顶部空白处
function modifyBook(li) {
	var oTitle = document.getElementById("btitle");
	var oAuth = document.getElementById("bauth");
	var oID = document.getElementById("bid");
	var oPage = document.getElementById("bpage");
	var oFrom = document.getElementById("bfrom");

	oTitle.value = document.querySelector("#"+li.id+">.btitle").innerText;
	oAuth.value = document.querySelector("#"+li.id+">.bauth").innerText;
	oID.value = document.querySelector("#"+li.id+">.bid").innerText;
	oPage.value = document.querySelector("#"+li.id+">.bpage").innerText;
	oFrom.value = document.querySelector("#"+li.id+">.bfrom").innerText;
}

//import/index.php中用于删除导入小说时点击小说名称后将信息自动填写到顶部空白处
function modifyImportBook(li) {
	var oUrl = document.getElementById("url");
	var oTable = document.getElementById("table");

	oUrl.value = document.querySelector("#"+li.id+">.burl").innerText;
	oTable.value = document.querySelector("#"+li.id+">.btable").innerText;
}

//manage.interface中用于切换小说来源时自动生成小说首页信息
//input栏隐藏，但功能必须保留
function fillPage(oFrom) {
	oPage = document.getElementById("bpage");
	oId = document.getElementById("bid");

	if (oFrom.value == "lq_list") {
		oPage.value = oId.value + "_1/";
	} else if (oFrom.value == "zyg_list") {
		oPage.value = oId.value + "/1";
	} else if (oFrom.value == "dd_list") {
		oPage.value = oId.value + "-1.html";
	} else if (oFrom.value == "tt_list") {
		oPage.value = "wapbook-" + oId.value + "_1/";
	} else if (oFrom.value == "mf_list") {
		oPage.value = oId.value + "_1/";
	} else if (oFrom.value == "80_list") {
		oPage.value = oId.value + "/page-1.html";
	}
}

//manage.interface中用于输入小说编号时自动生成小说首页信息
function fillPageById(oId) {
	if (typeof(oId) === "object") {oId = oId.value;}

	var oPage = document.getElementById("bpage");
	var oFrom = document.getElementById("bfrom");

	if (oFrom.value == "lq_list") {
		oPage.value = oId + "_1/";
	} else if (oFrom.value == "zyg_list") {
		oPage.value = oId + "/1";
	} else if (oFrom.value == "dd_list") {
		oPage.value = oId + "-1.html";
	} else if (oFrom.value == "tt_list") {
		oPage.value = "wapbook-" + oId + "_1/";
	} else if (oFrom.value == "mf_list") {
		oPage.value = oId + "_1/";
	} else if (oFrom.value == "80_list") {
		oPage.value = oId + "/page-1.html";
	}
}

function doGotoPage(from, page) {
	var query_url="./function/dogotopage.php";

	xmlHttp = GetXmlHttpObject();

	if (xmlHttp ==null) {
  		alert("Browser does not support HTTP Request");
		return;
	}

	query_url = query_url + "?from=" + from + "&page=" + page;
	xmlHttp.onreadystatechange = function () {stateChanged("gotopage")};
	xmlHttp.open("GET", query_url, true);
	xmlHttp.send(null);
}

//import/index.interface中用于导入小说内容并输出分析后的结果
//function/doimport.php用于分析小说内容
function doImport(url) {
	var import_url="../function/doimport.php";
	var include_catalog = document.getElementById("catalog").checked;
	var rnd = Math.floor(Math.random()*1000);

	xmlHttp = GetXmlHttpObject();

	if (xmlHttp ==null) {
  		alert("Browser does not support HTTP Request");
		return;
	}

	if (getArgs().length > 0) {
		rnd = "&t=" + rnd;
	} else {
		rnd = "?t=" + rnd;
	}
	//rnd = "";

	import_url = import_url + "?url=" + url + rnd + "&include_catalog=" + include_catalog;
	xmlHttp.onreadystatechange = function () {stateChanged("import")};
	xmlHttp.open("GET", import_url, true);
	xmlHttp.send(null);
}

//import/index.interface中用于分析完导入的小说内容后保存该信息
//保存分两步：
//		首先由doAppend函数生成临时文件,里面包含错误识别的章节目录
//		再由import/index.php根据临时文件生成正确的章节目录配置表并保存到import/table里
function doAppend(url) {
	var import_url="../function/doappend.php";
	var chapter_list = document.all.chapter_list;
	var catalog_list = document.all.catalog_list;
	var include_catalog = document.getElementById("catalog").checked;
	var url = document.getElementById("url").value;
	var rnd = Math.floor(Math.random()*1000);
	var chapters = [];
	var catalogs = [];

	//非合辑小说
	if (include_catalog != false) {
		//合辑小说，但是只有一部。。。。。
		if (typeof(catalog_list[0]) == "undefined") {
			if (!catalog_list.checked) {
				catalogs.push(catalog_list.getAttribute("text"));
			}
		} else {
			for (var i = 0; i < catalog_list.length; i++)
			{
				if (!catalog_list[i].checked) {
					catalogs.push(catalog_list[i].getAttribute("text"));
				}
			}
		}	
	}

	//什么变态小说。。。。只有一章
	if (typeof(chapter_list[0]) == "undefined") {
		if (!chapter_list.checked) {
			chapters.push(chapter_list.getAttribute("text"));
		}
	} else {
		for (var i = 0; i < chapter_list.length; i++)
		{
			if (!chapter_list[i].checked) {
				chapters.push(chapter_list[i].getAttribute("text"));
			}
		}
	}

	xmlHttp = GetXmlHttpObject();

	if (xmlHttp ==null) {
  		alert("Browser does not support HTTP Request");
		return;
	}

	if (getArgs().length > 0) {
		rnd = "&t=" + rnd;
	} else {
		rnd = "?t=" + rnd;
	}
	
	import_url = import_url + "?url=" + url + "&random=" + rnd + "&catalogs=" + catalogs + "&chapters=" + chapters + "&include_catalog=" + include_catalog;
	xmlHttp.onreadystatechange = function () {stateChanged("append")};
	xmlHttp.open("GET", import_url, true);
	xmlHttp.send(null);
}

//manage.interface中用于根据小说编号查询小说信息并填入顶部空白处
//function/doquery.php用于查询小说标题等信息
function doQuery(bid, bfrom) {
	var query_url="./function/doquery.php";

	xmlHttp = GetXmlHttpObject();

	if (xmlHttp ==null) {
  		alert("Browser does not support HTTP Request");
		return;
	}

	query_url = query_url + "?bid=" + bid + "&bfrom=" + bfrom;
	xmlHttp.onreadystatechange = function () {stateChanged("query");}
	xmlHttp.open("GET", query_url, true);
	xmlHttp.send(null);
}

//用于接收doImport、doQuery调用结果并执行相应后续操作
function stateChanged(type) {
	if (xmlHttp.readyState === 4 && xmlHttp.status === 200) {
		if (type == "import") {
			var contents = xmlHttp.responseText.split("###SPLIT###");

			if (contents != "") {
				document.getElementById("result_header").innerHTML = contents[0];
		 		document.getElementById("result_content").innerHTML = contents[1];
 				document.getElementById("result_section").style.display = "block";	
			}
		} else if (type == "query") {
			document.getElementById("query-btn").disabled = false;
			document.getElementById("append-btn").disabled = false;
			document.getElementById("delete-btn").disabled = false;
			//alert(xmlHttp.responseText);
			fillInfo(xmlHttp.responseText);
		} else if (type == "append") {
			window.location.href = "./index.php?temp=" + xmlHttp.responseText;
		} else if (type == "gotopage") {
			var json = JSON.parse(xmlHttp.responseText);
			var from = json.from;

			//if (from == "dd_list") {
				var pages = document.getElementById(from + "_page");
				var lists = document.getElementById(from + "s");
				//var count = lists.children.length - 1;
				var newEl = document.createElement("div");

				//for (var i = 0; i < count; i++) {
				lists.removeChild(lists.children[0]);
				//}

				newEl.innerHTML = json.output;
				lists.insertBefore(newEl, pages);
			//}
		}
 	}
}

//创建异步调用对象
function GetXmlHttpObject() {
	var xmlHttp = null;

	try
	{
		xmlHttp = new XMLHttpRequest();
	} catch (e) {
 		try
		{
			xmlHttp = new ActiveXObject("Msxml2.XMLHTTP");
		} catch (e) {
  			xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
	}

	return xmlHttp;
}

function doProhibit() {
	if(window.Event) document.captureEvents(Event.MOUSEUP);

	function nocontextmenu() {
   		event.cancelBubble = true 
   		event.returnvalue = false;

   		return false;
	}
	
	function norightclick(e) {
   		if (window.Event) {
      		if (e.which == 2 || e.which == 3)
      		return false;
   		} else if (event.button == 2 || event.button == 3) {
			event.cancelBubble = true 
			event.returnvalue = false;
			return false;
   		}
	}
	document.oncontextmenu = nocontextmenu;
	document.onmousedown = norightclick;
}

/* 以下函数用于article.interface中关于页面字体、背景颜色等的设置 */

var checkbg = "#A7A7A7";

//内容页用户设置
function nr_setbg(intype) {
	var huyandiv = document.getElementById("huyandiv");
	var light = document.getElementById("lightdiv");
	
	if (intype == "huyan") {
		if (huyandiv.style.backgroundColor == "") {
			set("light","huyan");
			setCookie("light=huyan;path=/");
		} else {
			set("light","no");
			setCookie("light=no;path=/");
		}
	} else if (intype == "light") {
		if (light.innerHTML == "关灯") {
			set("light","yes");
			setCookie("light=yes;path=/");
		} else {
			set("light","no");
			setCookie("light=no;path=/");
		}
	} else if (intype == "big") {
		set("font","big");
		setCookie("font=big;path=/");
	} else if (intype == "middle") {
		set("font","middle");
		setCookie("font=middle;path=/");
	} else if (intype == "small") {
		set("font","small");
		setCookie("font=small;path=/");
	}			
}

//内容页读取设置
function getset() {
	var strCookie=document.cookie;
	var arrCookie=strCookie.split("; ");
	var light;
	var font;

	for (var i=0; i < arrCookie.length; i++) {
		var arr = arrCookie[i].split("=");
		if ("light" == arr[0]) {
			light = arr[1];
			break;
		}
	}

	for (var i=0; i < arrCookie.length; i++) {
		var arr = arrCookie[i].split("=");
		if ("font" == arr[0]) {
			font = arr[1];
			break;
		}
	}
	
	//light
	if (light == "yes") {
		set("light","yes");
	} else if (light == "no") {
		set("light","no");
	} else if (light == "huyan") {
		set("light","huyan");
	}
	//font
	if (font == "big") {
		set("font","big");
	} else if (font == "middle") {
		set("font","middle");
	} else if (font == "small") {
		set("font","small");
	} else {
		set("","");	
	}
}

//内容页应用设置
function set(intype, p) {
	var nr_body = document.getElementById("nr_body");//页面body
	var huyandiv = document.getElementById("huyandiv");//护眼div
	var lightdiv = document.getElementById("lightdiv");//灯光div
	var fontbig = document.getElementById("fontbig");//大字体div
	var fontmiddle = document.getElementById("fontmiddle");//中字体div
	var fontsmall = document.getElementById("fontsmall");//小字体div
	var nr1 = document.getElementById("nr1");//内容div
	var nr_title = document.getElementById("nr_title");//文章标题
	//var pb_prev = document.getElementById("pb_prev");
	//var pb_mulu = document.getElementById("pb_mulu");
	//var pb_next = document.getElementById("pb_next");
	var postnavi = document.getElementById("postnavi");
	
	//灯光
	if (intype == "light") {
		if (p == "yes") {	
			//关灯
			lightdiv.innerHTML = "开灯";
			nr_body.style.backgroundColor = "#000";
			huyandiv.style.backgroundColor = "";
			nr_title.style.color = "#999";
			//pb_prev.style.color = "#999";
			//pb_mulu.style.color = "#999";
			//pb_next.style.color = "#999";
			nr1.style.color = "#999";
		} else if (p == "no") {
			//开灯
			lightdiv.innerHTML = "关灯";
			nr_body.style.backgroundColor = "#f4f4f4";
			nr1.style.color = "#000";
			nr_title.style.color = "#000";
			huyandiv.style.backgroundColor = "";
		} else if (p == "huyan") {
			//护眼
			lightdiv.innerHTML = "关灯";
			huyandiv.style.backgroundColor = checkbg;
			nr_body.style.backgroundColor = "#a8e2a5";
			nr_title.style.color = "#000";
			nr1.style.color = "#000";
		}
	}
	//字体
	if (intype == "font") {
		fontbig.style.backgroundColor = "";
		fontmiddle.style.backgroundColor = "";
		fontsmall.style.backgroundColor = "";
		if (p == "big") {
			fontbig.style.backgroundColor = checkbg;
			nr1.style.fontSize = "20px";
		} else if (p == "middle") {
			fontmiddle.style.backgroundColor = checkbg;
			nr1.style.fontSize = "18px";
		} else if (p == "small") {
			fontsmall.style.backgroundColor = checkbg;
			nr1.style.fontSize = "16px";
		}
	}
}