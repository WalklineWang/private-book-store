<?php
include "../function/function.php";
include "../function/conn.php";
session_start();

if (!isset($_SESSION["uid"])) {
	header ("location: ../");
	exit();
}

if (!isset($_GET["bid"]) || empty($_GET["bid"]) || !isset($_GET["btable"]) || empty($_GET["btable"]) || !isset($_GET["page"]) || empty($_GET["page"])) {// !isset($_GET["burl"]) || empty($_GET["burl"]) || !isset($_GET["include_catalog"]) || empty($_GET["include_catalog"]) ||  || !isset($_GET["title"]) || empty($_GET["title"])) {
	header ('location: ./');
}

$uid = $_SESSION["uid"];
$bid = $_GET["bid"];
$btable = $_GET["btable"];
$json = json_decode(file_get_contents("./table/" . mb_convert_encoding($btable, "gbk", "utf-8")), true);
$burl = $json["url"] . $json["random"];
$title = $json["title"];
$include_catalog = $json["include_catalog"];
$catalogs = $json["catalogs"];
$chapters = $json["chapters"];
$encodedtitle = urlencode($title);
$cataloglink = "./home.php?bid={$bid}&btable={$btable}";
$page = explode("_", $_GET["page"]);
$page1 = $_GET["page"];

$output = file_get_contents("./interface/article.interface");

if (!$file = @file($burl)) {echo "找不到源文件！"; return;}

//取小说正文内容
$contents = "";
for ($count = $page[0] + 1; $count < $page[1]; $count++)
{
	if ($file[$count] != "\r\n") {
		$contents .= "<p>{$file[$count]}</p>\n";	
	}
}

//章节标题
$atitle = rtrim(ltrim($file[$page[0]]));

$json = json_decode(file_get_contents("./table/" . mb_convert_encoding($btable, "gbk", "utf-8")), true);
$chapters = $json["chapters"];

//不分部小说，取上一章和下一章目录
if (!isset($_GET["catalog"])) {
	$output = str_replace("###CATALOGTITLE###", $title, $output);
	for ($count = 0; $count < count($chapters); $count++)
	{
		if (in_array($page[0], $chapters[$count])) {
			if ($count == 0) {
				$ptitle = "<a href='./home.php?bid={$bid}&btable={$btable}'>上一章</a>";
				break;
			} else {
				$ptitle = "<a href='./article.php?bid={$bid}&btable={$btable}&page={$chapters[$count - 1][1]}_{$chapters[$count][1]}'>上一章</a>";
				break;
			}
		}
	}

	for ($count = 0; $count < count($chapters); $count++)
	{
		if (in_array($page[1], $chapters[$count])) {
			if ($count == count($chapters) - 1) {
				$ntitle = "<a href='./home.php?bid={$bid}&btable={$btable}'>下一章</a>";
				break;
			} else {
				$ntitle = "<a href='./article.php?bid={$bid}&btable={$btable}&page={$chapters[$count][1]}_{$chapters[$count + 1][1]}'>下一章</a>";
				break; 
			}
		}
	}
} else {
	//分部小说，取上一章和下一章目录
	$catalog = $_GET["catalog"];
	$cataloglink = "./home.php?bid={$bid}&btable={$btable}&catalog={$catalog}";
	$output = str_replace("###CATALOGTITLE###", $catalogs[$catalog][0], $output);

	for ($count = 0; $count < count($chapters[$catalog]); $count++)
	{
		if (in_array($page[0], $chapters[$catalog][$count])) {
			if ($count == 0) {
				$ptitle = "<a href='./home.php?bid={$bid}&btable={$btable}&catalog={$catalog}'>上一章</a>";
				break;
			} else {
				$ptitle = "<a href='./article.php?bid={$bid}&btable={$btable}&page={$chapters[$catalog][$count - 1][1]}_{$chapters[$catalog][$count][1]}&catalog={$catalog}'>上一章</a>";
				break;
			}
		}
	}

	for ($count = 0; $count < count($chapters[$catalog]); $count++)
	{
		if (in_array($page[1], $chapters[$catalog][$count])) {
			if ($count == count($chapters[$catalog]) - 1) {
				$ntitle = "<a href='./home.php?bid={$bid}&btable={$btable}&catalog={$catalog}'>下一章</a>";
				break;
			} else {
				$ntitle = "<a href='./article.php?bid={$bid}&btable={$btable}&page={$chapters[$catalog][$count][1]}_{$chapters[$catalog][$count + 1][1]}&catalog={$catalog}'>下一章</a>";
				break; 
			}
		}
	}
}

unset($file);

$output = str_replace("###TITLE###", $title, $output);
$output = str_replace("###ARTICLETITLE###", $atitle, $output);
$output = str_replace("###CONTENTS###", $contents, $output);
$output = str_replace("###PREVTITLE###", $ptitle, $output);
$output = str_replace("###NEXTTITLE###", $ntitle, $output);
$output = str_replace("###CATALOGLINK###", $cataloglink, $output);
$output = str_replace("###CATALOG###", "<a href='./home.php?bid={$bid}&btable={$btable}'>目录</a>", $output);

echo $output;

if (!isset($_GET["catalog"])) {
	$result = mysql_query("select * from book_history where bid='dr-$bid' and uid='$uid' and bfrom='dr_list' limit 1");

	if (mysql_num_rows($result)) {
		mysql_query("update book_history set btitle='$atitle', burl='./article.php?bid=$bid&btable=$btable&page=$page1' where bid='dr-$bid' and uid='$uid' and bfrom='dr_list' limit 1");
		mysql_query("update book_import set last_read=CURRENT_TIMESTAMP where id='$bid' and uid='$uid' limit 1");
	} else {
		mysql_query("insert into book_history (bid, btitle, burl, bfrom, uid) values ('dr-$bid', '$atitle', './article.php?bid=$bid&btable=$btable&page=$page1', 'dr_list', '$uid')");
		mysql_query("update book_import set last_read=CURRENT_TIMESTAMP where id='$bid' and uid='$uid' limit 1");
	}
} else {
	$result = mysql_query("select * from book_history where bid='dr-$bid-$catalog' and uid='$uid' and bfrom='dr_list' limit 1");

	if (mysql_num_rows($result)) {
		mysql_query("update book_history set btitle='$atitle', burl='./article.php?bid=$bid&btable=$btable&page=$page1&catalog=$catalog' where bid='dr-$bid-$catalog' and uid='$uid' and bfrom='dr_list' limit 1");
		mysql_query("update book_import set last_read=CURRENT_TIMESTAMP where id='$bid' and uid='$uid' limit 1");
	} else {
		mysql_query("insert into book_history (bid, btitle, burl, bfrom, uid) values ('dr-$bid-$catalog', '$atitle', './article.php?bid=$bid&btable=$btable&page=$page1&catalog=$catalog', 'dr_list', '$uid')");
		mysql_query("update book_import set last_read=CURRENT_TIMESTAMP where id='$bid' and uid='$uid' limit 1");
	}
}

mysql_free_result($result);
mysql_close($conn);
?>