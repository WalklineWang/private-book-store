<?php
include "../function/conn.php";
include "../function/function.php";
session_start();

if (!isset($_SESSION["uid"])) {
	header ("location: ../");
	exit();
}

if (!isAdmin()) {
	header ("location: ../");
	exit();
}

$uid = $_SESSION["uid"];

//通过submit提交的命令有删除书籍
if (!isset($_POST["submit"])) {
	//传入需要过滤的章节内容，生成章节目录对照表
	if (isset($_GET["temp"]) && !empty($_GET["temp"])) {
		$json = json_decode(file_get_contents(base64_decode($_GET["temp"])), true);
		$url = $json["url"];
		$random = $json["random"];
		$include_catalog = $json["include_catalog"];
		$catalogs = $json["catalogs"];
		$chapters = $json["chapters"];

		unlink(base64_decode($_GET["temp"]));
		exportCatalogs($url, $random, $catalogs, $chapters, $include_catalog);

		header("location: ./");
		exit();	
	}

	$booklistdr = "(空)";
	$result = mysql_query("select * from book_import where uid='$uid'");

	if (mysql_num_rows($result) > 0) {
		$booklistdr = "";
		while ($row = mysql_fetch_assoc($result))
		{
			$booklistdr .= "
				<div class='s_list' id='dr" . $row["id"] . "' onClick='modifyImportBook(this);'>
					<div class='pt-name'>" . $row["btitle"] . "</div>
					<div class='pt-author' style='margin-bottom:0px;'>" . $row["bauth"] . "</div>
					<div class='pt-author ptm-hide burl'>" . $row["burl"] . "</div>
					<div class='pt-author ptm-hide btable'>" . $row["btable"] . "</div>
				</div>";
		}
	}

	$output = file_get_contents("./interface/index.interface");
	$output = str_replace("###CONTENT###", "", $output);
	$output = str_replace("###BOOKLISTDR###", $booklistdr, $output);

	echo $output;

	if (isset($_GET["status"]) && !empty($_GET["status"])) {
	    echo "<script language='javascript'>showTips('{$_GET['status']}')</script>";
    }
} else {
	if (isset($_POST["delete"]) && isset($_POST["url"]) && !empty($_POST["url"]) && isset($_POST["table"]) && !empty($_POST["table"])) {
		if ($_POST["delete"] == "true") {
			$result = mysql_query("delete from book_import where burl='" . $_POST["url"] . "' and uid='$uid'");
			unlink("./table/" . mb_convert_encoding($_POST["table"], "gbk", "utf-8"));

			header ("location: ./?status=success");
			exit();
		}
	} else {
		header ("location: ./");
		exit();
	}
}

mysql_free_result($result);
mysql_close($conn);

function exportCatalogs($url, $random, $catalog_remove, $chapter_remove, $include_catalog) {
	if (!$file = @file($url . $random)) {echo null; return;}

	//$size = filesize($url);
	$catalogs = array();
	$chapters = array();
	$chapters_new = array();
	$btitle = "";
	$bauth = "";
	$count = 0;

	foreach($file as $line)
	{
		$line = ltrim(rtrim($file[$count]));

		if (startWith($line, "###NAME###")) {
			$btitle = mb_substr($line, 10, strlen($line));
		}

		if (startWith($line, "###AUTHOR###")) {
			$bauth = mb_substr($line, 12, strlen($line));
		}

		if ($include_catalog != "false") {
			if (preg_match("/(第|卷){1}[一二三四五六七八九零十百千0-9]{1,30}(部|卷){1}/", $line, $null)) {
				if (!in_array($count, $catalog_remove)) {
					array_push($catalogs, array($line, $count));
				}
			}
		} else {
			if (preg_match("/第[一二三四五六七八九零十百千0-9]{1,30}(章|节|篇){1}/", $line, $null)) {
				if (!in_array($count, $chapter_remove)) {
					array_push($chapters_new, array($line, $count));
				}
			}
		}

		$count += 1;
	}

	if ($include_catalog == "false") {
		array_push($chapters_new, array("###结尾###", $count));
	}

	if ($include_catalog != "false") {
		$count = 0;
		foreach($catalogs as $catalog)
		{
			if ($count < count($catalogs) - 1) {
				for ($i = $catalogs[$count][1]; $i < $catalogs[$count + 1][1]; $i++)
				{
					$line = rtrim($file[$i]);

					if (preg_match("/第[一二三四五六七八九零十百千0-9]{1,30}(章|节|篇){1}/", $line, $null)) {
						if (!in_array($i, $chapter_remove)) {
							array_push($chapters, array($line, $i));
						}
					}
				}
				array_push($chapters, array("###结尾###", $i));
			} else {
				for ($i = $catalogs[$count][1]; $i < count($file); $i++)
				{
					$line = rtrim($file[$i]);

					if (preg_match("/第[一二三四五六七八九零十百千0-9]{1,30}(章|节|篇){1}/", $line, $null)) {
						if (!in_array($i, $chapter_remove)) {
							array_push($chapters, array($line, $i));
						}
					}
				}
				array_push($chapters, array("###结尾###", $i));
			}
			
			array_push($chapters_new, $chapters);
			$chapters = array();
			$count += 1;
		}
	}

	unset($file);

	$uid = $_SESSION["uid"];
	$btable = $uid . "_" . md5($btitle) . ".table";
	$filename = "../import/table/" . $btable;

	$temp = array(
				"url" => $url,
				"random" => $random,
				"include_catalog" => $include_catalog,
				"title" => $btitle,
				"author" => $bauth,
				"catalogs" => $catalogs,
				"chapters" => $chapters_new
	);

	file_put_contents($filename, json_encode($temp));

	$result = mysql_query("select * from book_import where uid='$uid' and burl='$url' limit 1");
	$rows = mysql_fetch_array($result);

	if ($result) {
    	if (mysql_num_rows($result) > 0) {
	    	mysql_query("update book_import set btitle='$btitle', bauth='$bauth', btable='$btable' where burl='$burl' and uid='$uid' limit 1");
    	} else {
	    	mysql_query("insert into book_import (burl, btitle, bauth, uid, btable) values ('$url', '$btitle', '$bauth', '$uid', '$btable')");
    	}

		header ("location: ./?status=success");
    	exit();
	} else {
    	header ("location: ./?status=failed");
	}

	mysql_free_result($result);
}
?>