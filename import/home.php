<?php
include "../function/function.php";
include "../function/conn.php";
session_start();

if (!isset($_SESSION["uid"])) {
	header ("location: ../");
	exit();
}

if (!isset($_GET["bid"]) || empty($_GET["bid"]) || !isset($_GET["btable"]) || empty($_GET["btable"])) {
	header("location: ./");
	exit();
}

$uid = $_SESSION["uid"];
$bid = $_GET["bid"];
$btable = $_GET["btable"];
$json = json_decode(file_get_contents("./table/" . mb_convert_encoding($btable, "gbk", "utf-8")), true);
$title = $json["title"];
$encodedtitle = urlencode($title);
$include_catalog = $json["include_catalog"];
$catalogs = $json["catalogs"];
$chapters = $json["chapters"];

//分部的小说
if ($include_catalog != "false") {
	//显示分部小说目录列表
	if (!isset($_GET["catalog"])) {
		$output = file_get_contents("./interface/home_catalogs.interface");
		$cataloglink = "../";

		$lists = "";
		for ($count = 0; $count < count($catalogs); $count++)
		{
			$lists .= "<div class='s_list'>
						<a href='./home.php?bid={$bid}&btable={$btable}&catalog={$count}'>{$catalogs[$count][0]}</a>\n";

			$result = mysql_query("select * from book_history where uid='$uid' and bid='dr-$bid-$count' and bfrom='dr_list' limit 1");
			
			if ($result_history = mysql_fetch_assoc($result)) {
				$lists .= "<a class='history' href='./" . $result_history["burl"] . "'>└当前阅读：《" . $result_history["btitle"] . "》</a>";
			}

			$lists .= "</div>";
			mysql_free_result($result);
		}

		$output = str_replace("###TITLE###", $title, $output);
		$output = str_replace("###CATALOGLINK###", $cataloglink, $output);
		$output = str_replace("###CATALOGLISTS###", $lists, $output);
	} else {
		//显示分部下的章节目录列表
		$output = file_get_contents("./interface/home.interface");
		$catalog = $_GET["catalog"];
		$cataloglink = "./home.php?bid={$bid}&btable={$btable}";

		$lists = "";
		for ($count = 0; $count < count($chapters[$catalog]) - 1; $count++)
		{
			$lists .= "<li><a href='./article.php?bid={$bid}&btable={$btable}&page={$chapters[$catalog][$count][1]}_{$chapters[$catalog][$count + 1][1]}&catalog={$catalog}'>{$chapters[$catalog][$count][0]}</a></li>";
		}

		$output = str_replace("###TITLE###", $title, $output);
		$output = str_replace("###CATALOGTITLE###", $catalogs[$catalog][0], $output);
		$output = str_replace("###CATALOGLINK###", $cataloglink, $output);
		$output = str_replace("###CATALOGLISTS###", $lists, $output);
	}
} else {
	//不分部的小说显示章节目录列表
	$output = file_get_contents("./interface/home.interface");
	$cataloglink = "../";
	$lists = "";
	for ($count = 0; $count < count($chapters) - 1; $count++)
	{
		$lists .= "<li><a href='./article.php?bid={$bid}&btable={$btable}&page={$chapters[$count][1]}_{$chapters[$count + 1][1]}'>{$chapters[$count][0]}</a></li>\n";
	}

	$output = str_replace("###TITLE###", $title, $output);
	$output = str_replace("###CATALOGTITLE###", $title, $output);
	$output = str_replace("###CATALOGLINK###", $cataloglink, $output);
	$output = str_replace("###CATALOGLISTS###", $lists, $output);
}

echo $output;

mysql_close($conn);
?>