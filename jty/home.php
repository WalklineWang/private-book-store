<?php
include "../function/function.php";
session_start();

if (!isset($_SESSION["uid"]) || !isset($_GET["title"]) || empty($_GET["title"])) {
	header ("location: ../");
	exit();
}

if (!isset($_GET["page"]) || empty($_GET["page"])) {
	$page = "";
} else {
	$page = "page/" . $_GET["page"];	
}

$title = $_GET["title"];
$encodedtitle = urlencode($title);
$param = "?am_force_theme_layout=mobile";
$url = "http://m.yangguiweihuo.com/0_1/all.html"; //{$page}{$param}";

if (!$homepage = getUrlData($url)) {
	header ("location: ../error.php?fromurl=" . urlencode($_SERVER['PHP_SELF'] . '?' . $_SERVER['QUERY_STRING']));
}

$output = file_get_contents("./interface/home.interface");

//取目录列表
preg_match_all("/<a href='\/0_1\/[\s\S]*?<\/a>/", $homepage, $cataloglists);

//最新章节列表
//$newlists = "";
//for ($i = 2; $i < 7; $i++)
//{
//	$newlists .= "<li>{$cataloglist[0][$i]}</li>\n";
//}

//全部章节列表
$lists = "";
foreach(array_reverse($cataloglists[0]) as $list)
{
	$lists .= "<li>{$list}</li>\n";
}
//$lists = "";
//for ($i = 7; $i < (count($cataloglist[0]) - 2); $i++)
//{
//	$lists .= "<li>{$cataloglist[0][$i]}</li>\n";
//}

//取翻页链接
//preg_match_all('/<div class="navigations[\s\S]*?<\/div>/', $homepage, $pages);
//preg_match_all('/<a href.*?<\/a>/', $pages[0][0], $pages);

//foreach($pages[0] as $page)
//{
//	if (mb_strpos($page, "page_previous")) {$ppage = str_replace("« ", "", str_replace("?am_force_theme_layout=mobile", "", $page));}
//	if (mb_strpos($page, "page_next")) {$npage = str_replace(" »", "", str_replace("?am_force_theme_layout=mobile", "", $page));}
//}

//if (empty($ppage)) {
//	$output = str_replace("###PAGES1###", "<div style='width:50%;text-align:center;' class='left'><a href='./home.php?title={$encodedtitle}'>首页</a></div>", $output);
//	$output = str_replace("###PAGES2###", "<div style='width:50%;text-align:center;' class='right'>{$npage}</div>", $output);
//	$output = str_replace("###PAGES3###", "", $output);
//} else if (empty($npage)) {
//	$output = str_replace("###PAGES1###", "<div style='width:50%;text-align:center;' class='left'><a href='./home.php?title={$encodedtitle}'>首页</a></div>", $output);
//	$output = str_replace("###PAGES2###", "<div style='width:50%;text-align:center;' class='right'>{$ppage}</div>", $output);
//	$output = str_replace("###PAGES3###", "", $output);
//} else {
//	$output = str_replace("###PAGES1###", "<div style='width:33%;text-align:center;' class='left'><a href='./home.php?title={$encodedtitle}'>首页</a></div>", $output);
//	$output = str_replace("###PAGES2###", "<div style='width:33%;text-align:center;' class='left'>{$ppage}</div>", $output);
//	$output = str_replace("###PAGES3###", "<div style='width:33%;text-align:center;' class='right'>{$npage}</div>", $output);
//}

$output = str_replace("###TITLE###", $title, $output);
//$output = str_replace("###NEWCATALOGLISTS###", $newlists, $output);
$output = str_replace("###CATALOGLISTS###", $lists, $output);

//$output = str_replace("http://www.yangguiweihuo.com/page/", "./home.php?title={$encodedtitle}&page=", $output);
//$output = str_replace("'http://www.yangguiweihuo.com/'", "./home.php?title={$encodedtitle}", $output);
$output = str_replace("/0_1/", "./article.php?title={$encodedtitle}&page=", $output);

echo $output;
?>