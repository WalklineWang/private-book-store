<?php
include "../function/function.php";
include "../function/conn.php";
session_start();

if (!isset($_SESSION["uid"])) {
	header ("location: ../");
	exit();
}

if (!isset($_GET["title"]) || empty($_GET["title"]) || !isset($_GET["page"]) || empty($_GET["page"])) {
	header ('location: ./');
}

$uid = $_SESSION["uid"];
$page = $_GET["page"];
$title = $_GET["title"];
$encodedtitle = urlencode($title);
//$param = "?am_force_theme_layout=mobile";
$url = "http://m.yangguiweihuo.com/0_1/{$page}";

if (!$article = file_get_contents($url)) {
	header ("location: ../error.php?fromurl=" . urlencode($_SERVER['PHP_SELF'] . '?' . $_SERVER['QUERY_STRING']));
}

$output = file_get_contents("./interface/article.interface");

//获取小说正文
preg_match('/<div id=\"chaptercontent[\s\S]*?<\/div>/', $article, $contents);
//preg_match('/<br\/>[\s\S].*<br\/>/', $article, $contents);
$contents = strip_tags($contents[0], "<br>");
$contents = str_replace("&nbsp;&nbsp;&nbsp;&nbsp;", "　　", $contents);
$contents = preg_replace('/<br\/>[\s\S]*?<br\/><br\/>/', "", $contents, 1);
//$contents = preg_replace('/<br\/><br\/>/', "", $contents, 1);

//preg_match_all('/<p>&nbsp;&nbsp;&nbsp;&nbsp;[\s\S]*?<\/p>/', $article, $contents);

//$contentlist = "";
//foreach($contents[0] as $content)
//{
//	$content = str_replace("&nbsp;&nbsp;&nbsp;&nbsp;", "　　", $content);
//	$contentlist .= $content . "\n";
//}

//获取章节名称
if (!$a = preg_match('/<span class=\"title\">[\s\S]*?<\/span>/', $article, $atitle)) {
	header ("location: ./home.php?from={$bfrom}&bid={$bid}&title={$encodedtitle}&page={$bid}-1.html");
	exit();
}
$atitle = strip_tags($atitle[0]);
$atitle = explode("&nbsp;&nbsp;", $atitle);
$atitle = $atitle[0];

//获取上一章节链接
preg_match("/<a href=[\s\S].*id=\"pt_prev\"[\s\S]*?<\/a>/", $article, $ptitle);
$ptitle = $ptitle[0];
//preg_match("/<span class=\"prev[\s\S]*?<\/span>/", $article, $ptitle);

//if (preg_match("/<a.*?<\/a>/", $ptitle[0], $ptitle)) {
//	$ptitle = preg_replace("/>.*?</", ">上一章<", $ptitle[0]);
//} else {
//	$ptitle = "<a href='./home.php?title={$encodedtitle}'>上一章</a>";
//}

//获取下一章节链接
preg_match("/<a href=[\s\S].*id=\"pt_next\"[\s\S]*?<\/a>/", $article, $ntitle);
$ntitle = $ntitle[0];
//preg_match("/<span class=\"next[\s\S]*?<\/span>/", $article, $ntitle);

//if (preg_match("/<a.*?<\/a>/", $ntitle[0], $ntitle)) {
//	$ntitle = preg_replace("/>.*?</", ">下一章<", $ntitle[0]);
//} else {
//	$ntitle = "<a href='./home.php?title={$encodedtitle}'>下一章</a>";
//}

$output = str_replace("###TITLE###", $title, $output);
$output = str_replace("###ARTICLETITLE###", $atitle, $output);
$output = str_replace("###CATALOGLINK###", "./home.php?title={$encodedtitle}", $output);
$output = str_replace("###PREVTITLE###", $ptitle, $output);
$output = str_replace("###NEXTTITLE###", $ntitle, $output);
$output = str_replace("###CONTENTS###", $contents, $output);
$output = str_replace("###CATALOG###", "<a href='./home.php?title={$encodedtitle}'>目录</a>", $output);
$output = str_replace("/0_1/", "./article.php?title={$encodedtitle}&page=", $output);

$output = str_replace("第(1/2)页,点击下一页继续阅读。" , "", $output);
$output = str_replace("『加入书签，方便阅读』" , "", $output);

echo $output;

$result = mysql_query("select * from book_history where bid='jty' and uid='$uid' and bfrom='jty' limit 1");

if (mysql_num_rows($result)) {
	mysql_query("update book_history set btitle='$atitle', burl='./jty/article.php?title=$title&page=$page' where bid='jty' and uid='$uid' and bfrom='jty' limit 1");
} else {
	mysql_query("insert into book_history (bid, btitle, burl, bfrom, uid) values ('jty', '$atitle', './jty/article.php?title=$title&page=$page', 'jty', '$uid')");
}

mysql_free_result($result);
mysql_close($conn);
?>