<?php
include "../function/function.php";
session_start();

if (!isset($_SESSION["uid"])) {
	exit();
}

if (!isset($_GET["bid"]) || empty($_GET["bid"]) || !isset($_GET["bfrom"]) || empty($_GET["bfrom"])) {
	exit();
}

$bid = $_GET["bid"];
$bfrom = $_GET["bfrom"];
queryBookInfo($bid, $bfrom);

//通过js异步调用，查询指定小说编号对应的小说名称等信息，返回结果由js处理并显示在manage.php中
function queryBookInfo($bid, $bfrom) {
	switch ($bfrom)
	{
		case "zyg_list":
			$url = "http://m.ziyouge.com/novel/" . $bid;

			if (!$homepage = getUrlData($url)) {
				header ("location: ./error.php?fromurl=" . urlencode($_SERVER['PHP_SELF'] . '?' . $_SERVER['QUERY_STRING']));
			}

			preg_match("/<title>.*?<\/title>/", $homepage, $info);
			$info = strip_tags($info[0]);
			$info = mb_split("\+", urlencode($info));
			$info = mb_split("%2F", $info[0]);
			$btitle = @urldecode($info[0]);
			$bauth = @urldecode($info[1]);

			break;
		case "lq_list":
			$url = "http://m.lequxs.com/" . substr($bid, 0, 2) . "/" . $bid . "/";

			if (!$homepage = mb_convert_encoding(getUrlData($url), "utf-8", "gbk")) {
				header ("location: ./error.php?fromurl=" . urlencode($_SERVER['PHP_SELF'] . '?' . $_SERVER['QUERY_STRING']));
			}

			preg_match("/<title.*?<\/title>/", $homepage, $info);
			$info = strip_tags($info[0]);
			$info = mb_split("\_", urlencode($info));
			$btitle = str_replace("最新章节", "", @urldecode($info[0]));
			$bauth = mb_substr(@urldecode($info[1]), 0, mb_strpos(@urldecode($info[1]), "-"));

			break;
		case "dd_list":
			$url = "http://m.dingdianzw.com/wapbook/" . $bid . ".html";

			if (!$homepage = getUrlData($url)) {
				header ("location: ./error.php?fromurl=" . urlencode($_SERVER['PHP_SELF'] . '?' . $_SERVER['QUERY_STRING']));
			}

			preg_match("/<title.*?<\/title>/", $homepage, $info);
			$info = strip_tags($info[0]);
			$info = mb_split("\)", $info);
			$info = mb_split("\(", $info[0]);
			$btitle = @urldecode($info[0]);
			$bauth = @urldecode($info[1]);

			break;
		case "tt_list":
			$url = "http://m.360118.com/" . $bid . "/";

			if (!$homepage = mb_convert_encoding(getUrlData($url), "utf-8", "gbk")) {
				header ("location: ./error.php?fromurl=" . urlencode($_SERVER['PHP_SELF'] . '?' . $_SERVER['QUERY_STRING']));
			}

			preg_match('/<a href="\/author.*?<\/a>/', $homepage, $bauth);
			$bauth = @strip_tags($bauth[0]);
			preg_match("/<h1 id=\"_52mb_h1.*?<\/h1>/", $homepage, $btitle);
			$btitle = @strip_tags($btitle[0]);

			break;
		case "mf_list":
			$url = "http://m.freexs.cn/wapbook/" . getShortId($bid) . "_" . $bid . ".html";

			if (!$homepage = mb_convert_encoding(getUrlData($url), "utf-8", "gbk")) {
				header ("location: ./error.php?fromurl=" . urlencode($_SERVER['PHP_SELF'] . '?' . $_SERVER['QUERY_STRING']));
			}
					
			preg_match('/<a href="\/author.*?<\/a>/', $homepage, $bauth);
			$bauth = @strip_tags($bauth[0]);
			preg_match("/<h1 id=\"_52mb_h1.*?<\/h1>/", $homepage, $btitle);
			$btitle = @strip_tags($btitle[0]);

			break;
		case "80_list":
			exit();
	}

	if (empty($bauth)) {
		$btitle = "查找无结果";
		$bauth = "查找无结果";
	}

	$result = array(
				"bid" => $bid,
				"bfrom" => $bfrom,
				"btitle" => $btitle,
				"bauth" => $bauth
	);

	echo json_encode($result);
}
?>