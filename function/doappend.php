<?php
include "../function/function.php";
session_start();

if (!isset($_SESSION["uid"])) {
	header ("../");
	exit();
}

if (!isset($_GET["chapters"]) || !isset($_GET["catalogs"]) || !isset($_GET["url"]) || empty($_GET["url"]) || !isset($_GET["random"]) || empty($_GET["random"]) || !isset($_GET["include_catalog"]) || empty($_GET["include_catalog"])) {
	header ("./");
	exit();
}

$url = $_GET["url"];
$random = $_GET["random"];
$catalogs = $_GET["catalogs"];
$chapters = $_GET["chapters"];
$include_catalog = $_GET["include_catalog"];

appendBookInfo($url, $random, $catalogs, $chapters, $include_catalog);

//通过js异步调用后，生成章节目录排除列表，结果返回给import/index.php处理
function appendBookInfo($url, $random, $catalogs, $chapters, $include_catalog) {
	$filename = "../import/table/temp/" . $_SESSION["uid"] . "_" . mt_rand(1000, 10000) . ".remove";
	$file = fopen($filename, "w") or die("Unable to open file!");

	$temp = array(
				"url" => $url,
				"random" => $random,
				"include_catalog" => $include_catalog,
				"catalogs" => $catalogs == "" ? array() : explode(",", $catalogs),
				"chapters" => $chapters == "" ? array() : explode(",", $chapters)
	);

	fwrite($file, json_encode($temp));
	fclose($file);

	echo base64_encode($filename);
}
?>