<?php
include "../function/function.php";
include_once "../function/conn.php";
session_start();

if (!isset($_SESSION["uid"])) {
	exit();
}

if ((!isset($_GET["from"]) || empty($_GET["from"])) && (!isset($_GET["page"]) || empty($_GET["page"]))) {
	exit();
}

$from = $_GET["from"];
$page = $_GET["page"];
gotoPage($from, $page);

function gotoPage($from, $page)
{
	$uid = $_SESSION["uid"];
	$output = "";

	switch ($from)
	{
		case "dr_list":
			$result = mysql_query("select * from book_import where uid='$uid' order by last_read desc limit $page, 3");

			if (mysql_num_rows($result) > 0) {
				while ($row = mysql_fetch_assoc($result))
				{
					$bid = $row["id"];
					$burl = $row["burl"];
					$btitle = $row["btitle"];
					$bfrom = "dr_list";
					$bauth = $row["bauth"];
					$btable = $row["btable"];

					$output .= "<div class='s_list'>
									<a href='./import/home.php?bid={$bid}&btable={$btable}'>{$bauth}：《{$btitle}》</a>";

					$result_history = mysql_query("select * from book_history where uid='$uid' and bid='dr-$bid' and bfrom='dr_list' limit 1");
			
					if ($row_history = mysql_fetch_assoc($result_history)) {
						$output .= "<a class='history' href='./import/{$row_history["burl"]}'>└当前阅读：《{$row_history["btitle"]}》</a>";
					}

					$output .= "</div>";
					mysql_free_result($result_history);
				}

				mysql_free_result($result);
			}

			break;		
		case "dd_list":
		case "tt_list":
		case "mf_list":
		case "80_list":
			$result = mysql_query("select * from book_list where bfrom='$from' and uid='$uid' order by last_read desc limit $page, 3");

			if (mysql_num_rows($result) > 0) {
				while ($row = mysql_fetch_assoc($result))
				{
					$bpage = $row["bpage"];
					$btitle = $row["btitle"];
					$bfrom = $row["bfrom"];
					$bauth = $row["bauth"];
					$bid = $row["bid"];
				
					$output .= "<div class='s_list'>
									<a href='./home.php?from={$bfrom}&bid={$bid}&page={$bpage}&title=" . urlencode($btitle) . "'>{$bauth}：《{$btitle}》</a>";

					$result_history = mysql_query("select * from book_history where uid='$uid' and bid='$bid' and bfrom='$bfrom' limit 1");
				
					if ($row_history = mysql_fetch_assoc($result_history)) {
						$output .= "<a class='history' href='{$row_history["burl"]}'>└当前阅读：《{$row_history["btitle"]}》</a>";
					}
				
					$output .= "</div>\n";
					mysql_free_result($result_history);
				}

				mysql_free_result($result);
			}
			
			break;
	}

	$result = array(
				"from" => $from,
				"output" => $output
	);

	echo json_encode($result);
}
?>