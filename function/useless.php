<?php
function getCatalogs2($url) {
	$output = file_get_contents("./interface/index.interface");
	if (!$file = @file($url)) {
		$output = str_replace("###CONTENT###", "", $output);
		echo $output;
		exit();
	}

	$content = file_get_contents("./interface/content.interface");
	$output = str_replace("###CONTENT###", $content, $output);

	$size = filesize($url);
	$catalogs = array();
	$chapters = array();
	$chapters_new = array();
	$novel_name = "";
	$author_name = "";
	$count = 0;

	foreach($file as $line)
	{
		$line = str_replace("\n", "", $file[$count]);

		if (startWith($line, "###NAME###")) {
			$novel_name = mb_substr($line, 10, strlen($line));
		}

		if (startWith($line, "###AUTHOR###")) {
			$author_name = mb_substr($line, 12, strlen($line));
		}

		if (preg_match("/第[一二三四五六七八九零十百千0-9]{1,30}部/", $line, $null)) {
			array_push($catalogs, array($line, $count));
		}

		$count += 1;
	}

	$count = 0;
	foreach($catalogs as $catalog)
	{
		if ($count < count($catalogs) - 1) {
			for ($i = $catalogs[$count][1]; $i < $catalogs[$count + 1][1]; $i++)
			{
				$line = str_replace("\n", "", $file[$i]);

				if (preg_match("/第[一二三四五六七八九零十百千0-9]{1,30}章/", $line, $null)) {
					array_push($chapters, array($line, $i));
				}
			}			
		} else {
			for ($i = $catalogs[$count][1]; $i < count($file); $i++)
			{
				$line = str_replace("\n", "", $file[$i]);

				if (preg_match("/第[一二三四五六七八九零十百千0-9]{1,30}章/", $line, $null)) {
					array_push($chapters, array($line, $i));
				}
			}
		}
		
		array_push($chapters_new, $chapters);
		$chapters = array();

		$count += 1;
	}

	unset($file);

	$content_title = $novel_name . " - " . $author_name;

	$count = 0;
	$content_list = "";
	foreach($catalogs as $catalog)
	{
		$content_list .= "<div class='s_list' style='padding: 10px 10px;'><label>" . $catalog[0] . " (" . $catalog[1] . ")</label>\n";

		foreach($chapters_new[$count] as $chapter)
		{
			$content_list .= "<label class='history history-label'>" . $chapter[0] . " (" . $chapter[1] . ")</label>\n";
		}

		$content_list .= "</div>\n";

		$count += 1;
	}

	$output = str_replace("###CONTENTTITLE###", $content_title, $output);
	$output = str_replace("###CONTENTLIST###", $content_list, $output);
	echo $output;
}

function getCatalogs1() {
	$file = file(filename);
	$size = filesize(filename);
	$catalogs = array();
	$chapters = array();
	$novel_name = "";
	$author_name = "";
	$count = 0;

	foreach($file as $line)
	{
		$line = str_replace("\n", "", $file[$count]);

		if (startWith($line, "###NAME###")) {
			$novel_name = mb_substr($line, 10, strlen($line));
		}

		if (startWith($line, "###AUTHOR###")) {
			$author_name = mb_substr($line, 12, strlen($line));
		}

		if (preg_match("/第[一二三四五六七八九零十百千0-9]{1,30}部/", $line, $null)) {
			array_push($catalogs, array($line, $count));
		}

		if (preg_match("/第[一二三四五六七八九零十百千0-9]{1,30}章/", $line, $null)) {
			array_push($chapters, array($line, $count));
		}

		$count += 1;
	}

	unset($file);

	echo "《" . "$novel_name" . "》\n";
	echo "作者：" . $author_name . "\n\n";

	echo "目录\n";

	foreach($catalogs as $value)
	{
		echo "\t" . $value[0] . "………………………………" . $value[1] . "\n";
	}

	foreach($chapters as $value)
	{
		echo "\t" . $value[0] . "………………………………" . $value[1] . "\n";
	}
}

function getCatalogs3() {
	$file = file(filename);
	$size = filesize(filename);
	$catalogs = array();
	$chapters = array();
	$chapters_new = array();
	$novel_name = "";
	$author_name = "";
	$count = 0;

	foreach($file as $line)
	{
		$line = str_replace("\n", "", $file[$count]);

		if (startWith($line, "###NAME###")) {
			$novel_name = mb_substr($line, 10, strlen($line));
		}

		if (startWith($line, "###AUTHOR###")) {
			$author_name = mb_substr($line, 12, strlen($line));
		}

		if (preg_match("/第[一二三四五六七八九零十百千0-9]{1,30}部/", $line, $null)) {
			array_push($catalogs, array($line, $count));
		}

		$count += 1;
	}

	$count = 0;
	foreach($catalogs as $catalog)
	{
		if ($count < count($catalogs) - 1) {
			for ($i = $catalogs[$count][1]; $i < $catalogs[$count + 1][1]; $i++)
			{
				$line = str_replace("\n", "", $file[$i]);

				if (preg_match("/第[一二三四五六七八九零十百千0-9]{1,30}章/", $line, $null)) {
					array_push($chapters, array($line, $i));
				}
			}			
		} else {
			for ($i = $catalogs[$count][1]; $i < count($file); $i++)
			{
				$line = str_replace("\n", "", $file[$i]);

				if (preg_match("/第[一二三四五六七八九零十百千0-9]{1,30}章/", $line, $null)) {
					array_push($chapters, array($line, $i));
				}
			}
		}
		
		array_push($chapters_new, $chapters);
		$chapters = array();

		$count += 1;
	}

	unset($file);

	echo "<h1>" . $novel_name . "</h1>\n";
	echo "<h2>" . $author_name . "</h2>\n";

	$count = 0;
	foreach($catalogs as $catalog)
	{
		echo "<p>" . $catalog[0] . "………………………………" . $catalog[1] . "</p>\n";

		foreach($chapters_new[$count] as $chapter)
		{
			echo "<p style='padding-left:25px;'>" . $chapter[0] . "………………………………" . $chapter[1] . "</p>\n";
		}

		$count += 1;
	}

	$count = 0;
	$file = fopen("./" . basename(filename, ".txt") . "_" . md5_file(filename) . "_" . $_SESSION["uid"] . ".table", "w") or die("Unable to open file!");
	$table_content = "TITLE:" . $novel_name . "\n";
	$table_content .= "AUTHOR:" . $author_name . "\n";
	$table_content .= "CATALOGBEGIN\n";
	foreach($catalogs as $catalog)
	{
		$table_content .= preg_replace("/\r/", "", $catalog[0]) . "###" . $catalog[1] . "\n";
	}
	$table_content .= "CATALOGEND\n";
	foreach($catalogs as $catalog)
	{
		$table_content .= "CHAPTERBEGIN" . $count . "\n";
		foreach($chapters_new[$count] as $chapter)
		{
			$table_content .= preg_replace("/\r/", "", $chapter[0]) . "###" . $chapter[1] . "\n";
		}
		$table_content .= "CHAPTEREND" . $count . "\n";
		$count += 1;
	}
	fwrite($file, $table_content);
	fclose($file);
}
?>