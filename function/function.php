<?php
function getUrlData($url) {
	ini_set("max_execution_time", 100);
	$ch = curl_init();

    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
   	curl_setopt($ch, CURLOPT_URL , $url);
   	curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 90);
   	curl_setopt($ch, CURLOPT_TIMEOUT, 90);
   	$res = curl_exec($ch);
	curl_close($ch);

	return $res;
}

function getShortId($bid) {
	if (strlen($bid) == 3) {
		$shortid = "0";
	} else if (strlen($bid) == 4) {
		$shortid = mb_substr($bid, 0, 1);
	} else if (strlen($bid) == 5) {
		$shortid = mb_substr($bid, 0, 2);
	} else {
		$shortid = mb_substr($bid, 0, 3);
	}

	return $shortid;
}

function isLocalhost() {return $isLocal = $_SERVER['SERVER_NAME'] === "localhost";}

function startWith($str, $pattern) {return mb_strpos($str, $pattern) === 0;}

function isAdmin() {return $_SESSION["uid"] == "Walkline" || $_SESSION["uid"] == "2169226201";}
?>