<?php
include "../function/function.php";
session_start();

if (!isset($_SESSION["uid"])) {
	header ("../");
	exit();
}

if (!isset($_GET["url"]) || empty($_GET["url"]) || !isset($_GET["include_catalog"]) || empty($_GET["include_catalog"])) {
	header ("../");
	exit();
}

$url = $_GET["url"];
$include_catalog = $_GET["include_catalog"];

getCatalogs($url, $include_catalog);

//通过js异步调用后，生成所有识别出的分部和章节目录列表，返回结果直接输出到import/index.php中
function getCatalogs($url, $include_catalog) {
	if (!$file = @file($url)) {echo null; return;}

	//$size = filesize($url);
	$catalogs = array();
	$chapters = array();
	$chapters_new = array();
	$novel_name = "";
	$author_name = "";
	$count = 0;

	foreach($file as $line)
	{
		//去除每行最后的换行
		$line = ltrim(rtrim($file[$count]));

		if (startWith($line, "###NAME###")) {
			$novel_name = mb_substr($line, 10, strlen($line));
		}

		if (startWith($line, "###AUTHOR###")) {
			$author_name = mb_substr($line, 12, strlen($line));
		}

		if ($include_catalog != "false") {
			if (preg_match("/(第|卷){1}[一二三四五六七八九零十百千0-9]{1,30}(部|卷){1}/", $line, $null)) {
				array_push($catalogs, array($line, $count));
			}
		} else {
			if (preg_match("/第[一二三四五六七八九零十百千0-9]{1,30}(章|节|篇){1}/", $line, $null)) {
				array_push($chapters, array($line, $count));
			}
		}

		$count += 1;
	}

	if ($include_catalog == "false") {
		array_push($chapters, array("###结尾###", $count));
	}

	if ($include_catalog != "false") {
		$count = 0;
		foreach($catalogs as $catalog)
		{
			if ($count < count($catalogs) - 1) {
				for ($i = $catalogs[$count][1]; $i < $catalogs[$count + 1][1]; $i++)
				{
					$line = rtrim($file[$i]); // str_replace(array("\n","\r"), "", $file[$i]);

					if (preg_match("/第[一二三四五六七八九零十百千0-9]{1,30}(章|节|篇){1}/", $line, $null)) {
						array_push($chapters, array($line, $i));
					}
				}
				array_push($chapters, array("###结尾###", $i));
			} else {
				for ($i = $catalogs[$count][1]; $i < count($file); $i++)
				{
					$line = rtrim($file[$i]); // str_replace(array("\n","\r"), "", $file[$i]);

					if (preg_match("/第[一二三四五六七八九零十百千0-9]{1,30}(章|节|篇){1}/", $line, $null)) {
						array_push($chapters, array($line, $i));
					}
				}
				array_push($chapters, array("###结尾###", $i));
			}

			array_push($chapters_new, $chapters);
			$chapters = array();
			$count += 1;
		}
	}

	unset($file);

	$content_title = $author_name . "：" . "《" . $novel_name . "》";

	$count = 0;
	$content_list = "";

	if ($include_catalog != "false") {
		foreach($catalogs as $catalog)
		{
			$content_list .= "<div style='padding: 10px 10px;'><label class='catalog-label'><input name='catalog_list' type='checkbox' style='vertical-align: middle;' checked='checked' text='{$catalog[1]}' />{$catalog[0]} ({$catalog[1]})</label>\n";

			foreach($chapters_new[$count] as $chapter)
			{
				if ($chapter[0] == "###结尾###") {
					$content_list .= "<label class='chapter-label'><input name='chapter_list' type='checkbox' style='vertical-align: middle;' checked='checked' text='{$chapter[1]}' /><font color='red'>{$chapter[0]} ({$chapter[1]})</font></label>\n";
				} else {
					$content_list .= "<label class='chapter-label'><input name='chapter_list' type='checkbox' style='vertical-align: middle;' checked='checked' text='{$chapter[1]}' />{$chapter[0]} ({$chapter[1]})</label>\n";
				}
			}

			$content_list .= "</div>\n";

			$count += 1;
		}
	} else {
		foreach($chapters as $chapter)
		{
			if ($chapter[0] == "###结尾###") {
				$content_list .= "<label class='chapter-label'><input name='chapter_list' type='checkbox' style='vertical-align: middle;' checked='checked' text='{$chapter[1]}' /><font color='red'>{$chapter[0]} ({$chapter[1]})</font></label>\n";
			} else {
				$content_list .= "<label class='chapter-label'><input name='chapter_list' type='checkbox' style='vertical-align: middle;' checked='checked' text='{$chapter[1]}' />{$chapter[0]} ({$chapter[1]})</label>\n";
			}
		}
	}

	echo $content_title . "###SPLIT###" . $content_list;
}
?>