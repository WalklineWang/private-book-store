<?php
include "./function/function.php";
session_start();

if (!isset($_SESSION["uid"])) {
	header ("location: ./");
	exit();
}

if (!isset($_GET["page"]) || !isset($_GET["from"]) || !isset($_GET["bid"]) || !isset($_GET["title"])) {
	header ("location: ./");
	exit();
}

$bfrom = $_GET["from"];
$page = $_GET["page"];
$bid = $_GET["bid"];
$title = $_GET["title"];
$encodedtitle = urlencode($title);

switch ($bfrom)
{
	case "zyg_list":
		$url = "http://m.ziyouge.com/novel/{$page}";
		
		if (!$homepage = getUrlData($url)) {
			//exit("无法打开资源网站，请稍后再试。。。。。。。");
			header ("location: ./error.php?fromurl=" . urlencode($_SERVER['PHP_SELF'] . '?' . $_SERVER['QUERY_STRING']));
		}

		$output = file_get_contents("./interface/home.interface");

		//取目录列表
		preg_match("/<ul[\s\S]*?<\/ul>/", $homepage, $cataloglists);
		$cataloglists = strip_tags($cataloglists[0], "<li><a>");
		//$cataloglists = str_replace("\r\n", "", $cataloglists);

		//取翻页链接
		//preg_match_all('/<div class=\"page.*?<\/div>/', $homepage, $pages);
		//preg_match_all('/<a href=.*?<\/a>/', $pages[0][0], $pages);

		/*
		if (count($pages[0]) > 3) {
			$output = str_replace("###PAGES1###", "<div class='catalognavigate left'>{$pages[0][0]}</div>", $output);
			$output = str_replace("###PAGES2###", "<div class='catalognavigate left'>{$pages[0][1]}</div>", $output);
			$output = str_replace("###PAGES3###", "<div class='catalognavigate left'>{$pages[0][2]}</div>", $output);
			$output = str_replace("###PAGES4###", "<div class='catalognavigate right'>{$pages[0][3]}</div>", $output);
		} else {
			$output = str_replace("###PAGES1###", "<div style='width:50%;text-align:center;display:inline;float:left;'>{$pages[0][0]}</div>", $output);
			$output = str_replace("###PAGES2###", "<div style='width:50%;text-align:center;display:inline;float:right;'>{$pages[0][1]}</div>", $output);
			$output = str_replace("###PAGES3###", "", $output);
			$output = str_replace("###PAGES4###", "", $output);
		}*/

		$output = str_replace("###TITLE###", $title, $output);
		$output = str_replace("###CATALOGLISTS###", $cataloglists, $output);
		$output = str_replace("/" . mb_substr($bid, 0, 2) . "/{$bid}/", "./article.php?from={$bfrom}&bid={$bid}&title={$encodedtitle}&page=", $output);
		$output = str_replace("/" . mb_substr($bid, 0, 2) . "/", "./home.php?from={$bfrom}&bid={$bid}&title={$encodedtitle}&page=", $output);

		break;
	case "lq_list":
		$url = "http://m.lequxs.com/" . substr($bid, 0, 2) . "/" . $page;
		
		if (!$homepage = mb_convert_encoding(getUrlData($url), "utf-8", "gbk")) {
			//exit("无法打开资源网站，请稍后再试。。。。。。。");
			header ("location: ./error.php?fromurl=" . urlencode($_SERVER['PHP_SELF'] . '?' . $_SERVER['QUERY_STRING']));
		}

		$output = file_get_contents("./interface/home.interface");

		//取目录列表
		preg_match("/<ul class=\"lb fk[\s\S]*?<\/ul>/", $homepage, $cataloglists);
		$cataloglists = strip_tags($cataloglists[0], "<li><a>");
		//$cataloglists = str_replace("\r\n", "", $cataloglists);

		//取翻页链接
		preg_match_all('/<div class=\"page.*?<\/div>/', $homepage, $pages);
		preg_match_all('/<a href=.*?<\/a>/', $pages[0][0], $pages);

		if (count($pages[0]) > 3) {
			$output = str_replace("###PAGES1###", "<div class='catalognavigate left'>{$pages[0][0]}</div>", $output);
			$output = str_replace("###PAGES2###", "<div class='catalognavigate left'>{$pages[0][1]}</div>", $output);
			$output = str_replace("###PAGES3###", "<div class='catalognavigate left'>{$pages[0][2]}</div>", $output);
			$output = str_replace("###PAGES4###", "<div class='catalognavigate right'>{$pages[0][3]}</div>", $output);
		} else {
			$output = str_replace("###PAGES1###", "<div style='width:50%;text-align:center;display:inline;float:left;'>{$pages[0][0]}</div>", $output);
			$output = str_replace("###PAGES2###", "<div style='width:50%;text-align:center;display:inline;float:right;'>{$pages[0][1]}</div>", $output);
			$output = str_replace("###PAGES3###", "", $output);
			$output = str_replace("###PAGES4###", "", $output);
		}

		$output = str_replace("###TITLE###", $title, $output);
		$output = str_replace("###CATALOGLISTS###", $cataloglists, $output);
		$output = str_replace("/" . mb_substr($bid, 0, 2) . "/{$bid}/", "./article.php?from={$bfrom}&bid={$bid}&title={$encodedtitle}&page=", $output);
		$output = str_replace("/" . mb_substr($bid, 0, 2) . "/", "./home.php?from={$bfrom}&bid={$bid}&title={$encodedtitle}&page=", $output);

		break;
	case "dd_list":
		$url = "http://m.dingdianzw.com/" . getShortId($bid) . "_" . $bid . "/all.html"; //{$page}";

		if (!$homepage = getUrlData($url)) {
			//exit("无法打开资源网站，请稍后再试。。。。。。。");
			header ("location: ./error.php?fromurl=" . urlencode($_SERVER['PHP_SELF'] . '?' . $_SERVER['QUERY_STRING']));
		}

		$output = file_get_contents("./interface/home.interface");

		//取目录列表
		preg_match_all('/<a href=\'\/wapbook\/[\s\S]*?<\/a>/', $homepage, $cataloglists);
		//$cataloglists = str_replace("onechapter'>", "", $cataloglists[0]);
		//$cataloglists = str_replace("\r\n", "", $cataloglists);
		
		$lists = "";
		foreach($cataloglists[0] as $list)
		{
			$lists .= "<li>{$list}</li>\n";
		}

		//取翻页链接
		//preg_match_all('/<td><a href=\'\/wapbook.*?<\/td>/', $homepage, $pages);

		//if (count($pages[0]) == 3) {
		//	$output = str_replace("###PAGES1###", "<div class='catalognavigate left'><a href='./home.php?from={$bfrom}&bid={$bid}&title={$encodedtitle}&page={$bid}-1.html'>首页</a></div>", $output);
		//	$output = str_replace("###PAGES2###", "<div class='catalognavigate left'>" . strip_tags($pages[0][0], "<a>") . "</div>", $output);
		//	$output = str_replace("###PAGES3###", "<div class='catalognavigate left'>" . strip_tags($pages[0][1], "<a>") . "</div>", $output);
		//	$output = str_replace("###PAGES4###", "<div class='catalognavigate right'>" . strip_tags($pages[0][2], "<a>") . "</div>", $output);
		//}

		$output = str_replace("<div id=\"postnavi\" style=\"display:block;\">", "<div id=\"postnavi\" style=\"display:none;\">", $output);

		$output = str_replace("###TITLE###", $title, $output);
		$output = str_replace("###CATALOGLISTS###", $lists, $output);

		$output = str_replace("/wapbook/{$bid}-", "./home.php?from={$bfrom}&bid={$bid}&title={$encodedtitle}&page={$bid}-", $output);
		$output = str_replace("/wapbook/{$bid}_", "./article.php?from={$bfrom}&bid={$bid}&title={$encodedtitle}&page=", $output);
		
		break;
	case "tt_list":
		$url = "http://m.360118.com/{$page}";
		
		if (!$homepage = mb_convert_encoding(getUrlData($url), "utf-8", "gbk")) {
			header ("location: ./error.php?fromurl=" . urlencode($_SERVER['PHP_SELF'] . '?' . $_SERVER['QUERY_STRING']));
		}
		
		$output = file_get_contents("interface/home.interface");

		//取目录列表
		preg_match("/<ul class=\"chapter[\s\S]*?<\/ul>/", $homepage, $cataloglists);
		$cataloglists = strip_tags($cataloglists[0], "<li><a>");
		//$cataloglists = ltrim(rtrim($cataloglists));
		$cataloglists = str_replace("\r\n", "\n", $cataloglists);
		$cataloglists = str_replace("\t", "", $cataloglists);

		//取翻页链接
		preg_match_all('/<a href="\/wapbook.*?<\/a>/', $homepage, $pages);

		if (count($pages[0]) > 5) {
			$output = str_replace("###PAGES1###", "<div class='catalognavigate left'>{$pages[0][2]}</div>", $output);
			$output = str_replace("###PAGES2###", "<div class='catalognavigate left'>{$pages[0][3]}</div>", $output);
			$output = str_replace("###PAGES3###", "<div class='catalognavigate left'>{$pages[0][4]}</div>", $output);
			$output = str_replace("###PAGES4###", "<div class='catalognavigate right'>{$pages[0][5]}</div>", $output);
		} else {
			$output = str_replace("###PAGES1###", "<div style='width:50%;text-align:center;display:inline;float:left;'>{$pages[0][2]}</div>", $output);
			$output = str_replace("###PAGES2###", "<div style='width:50%;text-align:center;display:inline;float:right;'>{$pages[0][3]}</div>", $output);
			$output = str_replace("###PAGES3###", "", $output);
			$output = str_replace("###PAGES4###", "", $output);
		}

		$output = str_replace("###TITLE###", $title, $output);
		$output = str_replace("###CATALOGLISTS###", $cataloglists, $output);
		$output = str_replace("/html/" . getShortId($bid) . "/{$bid}/", "./article.php?from={$bfrom}&bid={$bid}&title={$encodedtitle}&page=", $output);
		$output = str_replace("/wapbook-", "./home.php?from={$bfrom}&bid={$bid}&title={$encodedtitle}&page=wapbook-", $output);

		break;
	case "mf_list":
		$shortid = getShortId($bid);
		$url = "http://m.freexs.cn/{$shortid}/{$page}";
		
		if (!$homepage = mb_convert_encoding(getUrlData($url), "utf-8", "gbk")) {
			header ("location: ./error.php?fromurl=" . urlencode($_SERVER['PHP_SELF'] . '?' . $_SERVER['QUERY_STRING']));
		}
		
		$output = file_get_contents("interface/home.interface");

		//取目录列表
		preg_match("/<ul class=\"chapter[\s\S]*?<\/ul>/", $homepage, $cataloglists);
		$cataloglists = strip_tags($cataloglists[0], "<li><a>");
		//$cataloglists = ltrim(rtrim($cataloglists));
		$cataloglists = str_replace("\r\n", "\n", $cataloglists);
		$cataloglists = str_replace("\t", "", $cataloglists);

		//取翻页链接
		preg_match_all("/<a href=\"\/" . $shortid . ".*?<\/a>/", $homepage, $pages);

		if (count($pages[0]) > 4) {
			$output = str_replace("###PAGES1###", "<div class='catalognavigate left'>{$pages[0][1]}</div>", $output);
			$output = str_replace("###PAGES2###", "<div class='catalognavigate left'>{$pages[0][2]}</div>", $output);
			$output = str_replace("###PAGES3###", "<div class='catalognavigate left'>{$pages[0][3]}</div>", $output);
			$output = str_replace("###PAGES4###", "<div class='catalognavigate right'>{$pages[0][4]}</div>", $output);
		} else {
			$output = str_replace("###PAGES1###", "<div style='width:50%;text-align:center;display:inline;float:left;'>{$pages[0][1]}</div>", $output);
			$output = str_replace("###PAGES2###", "<div style='width:50%;text-align:center;display:inline;float:right;'>{$pages[0][2]}</div>", $output);
			$output = str_replace("###PAGES3###", "", $output);
			$output = str_replace("###PAGES4###", "", $output);
		}

		$output = str_replace("###TITLE###", $title, $output);
		$output = str_replace("###CATALOGLISTS###", $cataloglists, $output);
		$output = str_replace("/wapbook/{$shortid}_{$bid}_", "./article.php?from={$bfrom}&bid={$bid}&title={$encodedtitle}&page=", $output);
		$output = str_replace("/{$shortid}/{$bid}_", "./home.php?from={$bfrom}&bid={$bid}&title={$encodedtitle}&page={$bid}_", $output);

		break;
	case "80_list":
		//$url = "http://m.80txt.com/" . $page;
		
		//$output = mb_convert_encoding(getUrlData($url), "gbk", "utf-8");

		//替换字符集
		//$output = str_replace("charset=utf-8", "charset=gbk", $output);
		
		//替换样式表链接
		//$output = str_replace("/images/s.css", "./css/s.css", $output);

		//替换脚本代码
		//$output = preg_replace("/<div style=\"margin-bottom[\s\S]*?<\/div>/", "<!-- 替换脚本代码 -->", $output);
		//$output = preg_replace("/<div class=\"ml_n[\s\S]*?<\/div>/", "<!-- 替换脚本代码 -->", $output);
		//$output = preg_replace("/<script>[\s\S]*?<\/script>/", "<!-- 替换脚本代码 -->", $output);

		//替换页脚
		//$output = preg_replace("/<div class=\"footer[\s\S]*?<\/div>/", "<!-- 替换页脚 --><br />", $output);

		//替换页首按钮
		//$output = preg_replace("/<div class=\"link[\s\S]*?<\/div>/", "<!-- 替换页首按钮 -->", $output);

		//过滤下载提示
		//$output = preg_replace("/<span>下载时请选择[\s\S]*?<\/span>/", "<!-- 替换下载提示 -->", $output);

		//过滤顶部标题栏
		//$output = preg_replace("/<div class=\"header[\s\S]*?<\/div>/", "<!-- 过滤顶部标题栏 -->", $output);

		//增加返回首页链接
		//$output = str_replace("</h1>", "<div style='float:right;padding-right:10px;'><a href='./'>首页</a></div></h1>", $output);

		//过滤错误举报链接
		//$output = preg_replace("/<a rel=\"nofollow[\s\S]*?<\/a>/", "<!-- 替换过滤举报链接 -->", $output);

		//替换目录首页链接
		//$output = str_replace("\"/" . $bid . "/\"", "./home.php?from=" . $bfrom . "&bid=" . $bid . "&page=" . $bid . "/page-1.html&title=" . $encodedtitle, $output);
		//替换目录其它链接
		//$output = str_replace("/" . $bid . "/page-", "./home.php?title=" . $encodedtitle . "&from=" .$bfrom . "&bid=" . $bid . "&page=" . $bid . "/page-", $output);

		//替换文章链接
		//$output = str_replace("\"/" . $bid, "\"./article.php?title=" . $encodedtitle . "&from=" . $bfrom . "&bid=" . $bid . "&page=" . $bid, $output);
}

echo $output;
?>