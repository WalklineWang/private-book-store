<?php
include ("./function/conn.php");
include ("./function/function.php");
session_start();

if (!isset($_SESSION["uid"])) {
	header ("location: ./");
	exit();
}

$uid = $_SESSION["uid"];
$booklists_id = array("zyg", "lq", "dd", "tt", "mf", "bl"); //顶点中文网、天天中文网、免费小说网、八零电子书
$booklists_query = array("zyg_list", "lq_list", "dd_list", "tt_list", "mf_list", "80_list"); //顶点中文网、天天中文网、免费小说网、八零电子书
$booklists_content = array("(空)", "(空)", "(空)", "(空)", "(空)", "(空)"); //顶点中文网、天天中文网、免费小说网、八零电子书
$booklists_replacement = array("###BOOKLISTZYG###", "###BOOKLISTLQ###", "###BOOKLISTDD###", "###BOOKLISTTT###", "###BOOKLISTMF###", "###BOOKLIST80###"); //顶点中文网、天天中文网、免费小说网、八零电子书

//响应注销命令
if (isset($_GET["cmd"])) {
	if ($_GET["cmd"] == "logout") {
		session_unset();
		session_destroy();

		header("location: ./");
		exit();
	}
}

//submit提交的事件有添加和删除小说记录
if (!isset($_POST['submit'])) {
	$output = file_get_contents("./interface/manage.interface");

	//读取每个网站对应的小说列表
	for ($count = 0; $count < count($booklists_query); $count++)
	{
		$result = mysql_query("select * from book_list where uid='$uid' and bfrom='$booklists_query[$count]'");

		if (mysql_num_rows($result) > 0) {
			$booklists_content[$count] = "";
			
			while ($row = mysql_fetch_assoc($result))
			{
				$booklists_content[$count] .= "
				<div class='s_list' id='{$booklists_id[$count]}{$row["bid"]}' onClick='modifyBook(this);'>
					<div class='pt-name btitle'>{$row["btitle"]}</div>
					<div class='pt-author bauth' style='margin-bottom:0px;'>{$row["bauth"]}</div>
					<div class='pt-author bid' style='display:none;'>{$row["bid"]}</div>
					<div class='pt-author bpage ptm-hide'>{$row["bpage"]}</div>
					<div class='pt-author bfrom ptm-hide'>{$row["bfrom"]}</div>
				</div>";
			}
	    }
	}

	$import_button = "";
	//导入书籍功能只有管理员可用
	if (isAdmin()) {
		$import_button = file_get_contents("./interface/manage-import.interface");
		$output = str_replace("###IMPORTBUTTON###", $import_button, $output);
	}

	//显示每个网站对应的小说列表
	for ($count = 0; $count < count($booklists_content); $count++)
	{
		$output = str_replace($booklists_replacement[$count], $booklists_content[$count], $output);	
	}

    $output = str_replace("###IMPORTBUTTON###", $import_button, $output);

    echo $output;

    if (isset($_GET["status"]) && !empty($_GET["status"])) {
	    echo "<script language='javascript'>showTips('{$_GET['status']}')</script>";
    }
} else {
	//删除选定的小说
	if (isset($_POST["delete"]) && isset($_POST["bid"]) && !empty($_POST["bid"])) {
		if ($_POST["delete"] == "true") {
			mysql_query("delete from book_list where bid='" . $_POST["bid"] . "' and uid='$uid'");

			header ("location: ./manage.php?status=success");
			exit();
		}
	}

	//添加小说记录
	if (!isset($_POST["bid"]) || empty($_POST["bid"]) || !isset($_POST["btitle"]) || empty($_POST["btitle"]) || !isset($_POST["bpage"]) || empty($_POST["bpage"]) || !isset($_POST["bauth"]) || empty($_POST["bauth"]) || !isset($_POST["bfrom"]) || empty($_POST["bfrom"])) {
		header ("location: ./manage.php");
		exit();
	}

	$bid = $_POST["bid"];
	$bfrom = $_POST["bfrom"];
	$btitle = $_POST["btitle"];
	$bpage = $_POST["bpage"];
	$bauth = $_POST["bauth"];

	$result = mysql_query("select * from book_list where uid='$uid' and bid='$bid' and bfrom='$bfrom' limit 1");
	$rows = mysql_fetch_array($result);

	if ($result) {
    	if (mysql_num_rows($result) > 0) {
	    	mysql_query("update book_list set bid='$bid', btitle='$btitle', bpage='$bpage', bauth='$bauth', uid='$uid', bfrom='$bfrom', last_read=CURRENT_TIMESTAMP where bid='$bid' and uid='$uid' and bfrom='$bfrom' limit 1");
    	} else {
	    	mysql_query("insert into book_list (bid, bpage, btitle, bauth, uid, bfrom, last_read) values ('$bid', '$bpage', '$btitle', '$bauth', '$uid', '$bfrom', CURRENT_TIMESTAMP)");
    	}

    	header ("location: ./manage.php?status=success");
    	exit();
	} else {
    	header ("location: ./manage.php?status=failed");
	}
}

mysql_free_result($result);
mysql_close($conn);
?>