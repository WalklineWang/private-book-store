<?php
include "./function/conn.php";
include "./function/function.php";
session_set_cookie_params(24 * 3600 * 365, "/");
session_start();

$output = file_get_contents("./interface/index.interface");

$booklists_query = array("zyg_list", "lq_list", "dd_list", "tt_list", "mf_list"); //"80_list"); //顶点中文网、天天中文网、免费小说网、八零电子书
$booklists_content = array("(空)", "(空)", "(空)", "(空)", "(空)"); //, "(空)"); //顶点中文网、天天中文网、免费小说网、八零电子书
$booklists_replacement = array("###BOOKLISTZYG###", "###BOOKLISTLQ###", "###BOOKLISTDD###", "###BOOKLISTTT###", "###BOOKLISTMF###"); //"###BOOKLISTLQ###", "###BOOKLIST80###"); //顶点中文网、天天中文网、免费小说网、八零电子书
$booklist80 = "(空)";
$booklistdr = "(空)";
//$booklistzyg = "(空)";
$bookjty = "(空)";
$bookmdj = "";
$sharetoweibo = "";
$userinfo = "";

if (isset( $_SESSION["uid"])) {
	$uid = $_SESSION["uid"];

	//管理员登录后显示用户列表链接
	if (isAdmin()) {
		$userinfo = "<a class='ptm-pull-left' href='./info/' target='_blank'>统计</a>";
	}

	//读取每个网站对应的小说列表和历史记录
	for ($count = 0; $count < count($booklists_query); $count++)
	{
		$rscount = 0;
		$query = mysql_query("select count(*) from book_list where bfrom='$booklists_query[$count]' and uid='$uid' order by last_read desc");
		if ($rs = mysql_fetch_array($query)) {$rscount = $rs[0];}

		$result = mysql_query("select * from book_list where bfrom='$booklists_query[$count]' and uid='$uid' order by last_read desc limit 3");

		if (mysql_num_rows($result) > 0) {
			$booklists_content[$count] = "<div>";
			
			while ($row = mysql_fetch_assoc($result))
			{
				$bpage = $row["bpage"];
				$btitle = $row["btitle"];
				$bfrom = $row["bfrom"];
				$bauth = $row["bauth"];
				$bid = $row["bid"];
				
				$booklists_content[$count] .= "<div class='s_list'>
												<a href='./home.php?from={$bfrom}&bid={$bid}&page={$bpage}&title=" . urlencode($btitle) . "'>{$bauth}：《{$btitle}》</a>";

				$result_history = mysql_query("select * from book_history where uid='$uid' and bid='$bid' and bfrom='$bfrom' limit 1");
				
				if ($row_history = mysql_fetch_assoc($result_history)) {
					$booklists_content[$count] .= "<a class='history' href='{$row_history["burl"]}'>└当前阅读：《{$row_history["btitle"]}》</a>";
				}
				
				$booklists_content[$count] .= "</div>\n";
				mysql_free_result($result_history);
			}
			$booklists_content[$count] .= "</div>";

			mysql_free_result($result);

			if ($rscount > 3) {
				$booklists_content[$count] .= "<div id='" . $bfrom . "_page' class='s_list' style='height:30px;margin-top:-5px;'>";
	
				for ($rscounts = 0; $rscounts < ($rscount / 3); $rscounts++)
				{
					$booklists_content[$count] .= "<a href='javascript:void(0);' onclick='goto(\"{$bfrom}\", " . ($rscounts * 3) . ");' class='nav_pages'><span class='nav_span'>" . ($rscounts + 1) . "</span></a>\n";
				}

				$booklists_content[$count] .= "</div>";
			}
		}
	}

	//读取导入的书籍列表和历史记录
	$rscount = 0;
	$query = mysql_query("select count(*) from book_import where uid='$uid' order by last_read desc");
	if ($rs = mysql_fetch_array($query)) {$rscount = $rs[0];}

	$result = mysql_query("select * from book_import where uid='$uid' order by last_read desc limit 3");

	if (mysql_num_rows($result) > 0) {
		$booklistdr = "<div>";

		while ($row = mysql_fetch_assoc($result))
		{
			$bid = $row["id"];
			$burl = $row["burl"]; // . $row["rnd"];
			$btitle = $row["btitle"];
			$bfrom = "dr_list";
			$bauth = $row["bauth"];
			$btable = $row["btable"];

			$booklistdr .= "<div class='s_list'>
							<a href='./import/home.php?bid={$bid}&btable={$btable}'>{$bauth}：《{$btitle}》</a>";

			$result_history = mysql_query("select * from book_history where uid='$uid' and bid='dr-$bid' and bfrom='dr_list' limit 1");
			
			if ($row_history = mysql_fetch_assoc($result_history)) {
				$booklistdr .= "<a class='history' href='./import/{$row_history["burl"]}'>└当前阅读：《{$row_history["btitle"]}》</a>";
			}
			mysql_free_result($result_history);

			$booklistdr .= "</div>";
		}
		$booklistdr .= "</div>";

		mysql_free_result($result);

		if ($rscount > 3) {
			$booklistdr .= "<div id='" . $bfrom . "_page' class='s_list' style='height:30px;margin-top:-5px;'>";
	
			for ($rscounts = 0; $rscounts < ($rscount / 3); $rscounts++)
			{
				$booklistdr .= "<a href='javascript:void(0);' onclick='goto(\"{$bfrom}\", " . ($rscounts * 3) . ");' class='nav_pages'><span class='nav_span'>" . ($rscounts + 1) . "</span></a>\n";
			}

			$booklistdr .= "</div>";
		}
	}

	$bookjty = "<div class='s_list'>\n<a href='./jty/'>浮梦流年：《劫天运》</a>\n###JTYHISTORY###\n</div>";

	$jtyhistory = "";
	$result_history = mysql_query("select * from book_history where uid='$uid' and bid='jty' and bfrom='jty' limit 1");
	if ($row_history = mysql_fetch_assoc($result_history)) {
		$jtyhistory = "<a class='history' href='{$row_history["burl"]}'>└当前阅读：《{$row_history["btitle"]}》</a>";
	}
	mysql_free_result($result_history);

	$bookjty = str_replace("###JTYHISTORY###", $jtyhistory, $bookjty);
	$bookmdj = "<div class='s_list'>\n<a href='./mdj/'>耳东水寿：《民调局异闻录》</a>\n</div>";

	//分享到微博
	$weibourl = "http://widget.weibo.com/dialog/PublishWeb.php?default_text=%e6%88%91%e6%ad%a3%e5%9c%a8%e7%94%a8%40Walkline+%e7%9a%84%e7%a7%81%e4%ba%ba%e4%b9%a6%e5%ba%93%e9%98%85%e8%af%bb%e5%b0%8f%e8%af%b4%ef%bc%8c%e4%bd%a0%e4%b9%9f%e6%9d%a5%e8%af%95%e8%af%95%e5%90%a7%7e&uid=2169226201&refer=y&tag=%E7%A7%81%E4%BA%BA%E4%B9%A6%E5%BA%93&language=zh_cn&app_src=144q3D&button=pubilish";
	$sharetoweibo = "<p style='padding: 10px 0px;'><a href='{$weibourl}' target='_blank'><img src='./images/weibo.png' /></a></p>";	

	$manage = "<a class='ptm-pull-right' href='./manage.php'>管理</a>";
} else {
	$manage = "<a class='ptm-pull-right' href='./login.php'>登录</a>";
}

$output = str_replace("###USERINFO###", $userinfo, $output);
$output = str_replace("###MANAGE###", $manage, $output);

for ($count = 0; $count < count($booklists_content); $count++)
{
	$output = str_replace($booklists_replacement[$count], $booklists_content[$count], $output);	
}

$output = str_replace("###BOOKLIST80###", $booklist80, $output);
$output = str_replace("###BOOKLISTDR###", $booklistdr, $output);
$output = str_replace("###JIETIANYUN###", $bookjty, $output);
$output = str_replace("###MINDIAOJU###", $bookmdj, $output);
$output = str_replace("###SHARETOWEIBO###", $sharetoweibo, $output);

echo $output;

mysql_close($conn);
?>