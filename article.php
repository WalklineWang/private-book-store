<?php
include "./function/conn.php";
include "./function/function.php";
session_start();

if (!isset($_SESSION["uid"]) || !isset($_GET["from"]) && !empty($_GET["from"]) || !isset($_GET["bid"]) && !empty($_GET["bid"]) || !isset($_GET["title"])) {
	header ("location: ./");
	exit();
}

$uid = $_SESSION["uid"];
$bid = $_GET["bid"];
$title = $_GET["title"];
$encodedtitle = urlencode($title);
$bfrom = $_GET["from"];

//处理链接参数错误的跳转
if (!isset($_GET["page"]) || empty($_GET["page"]) || $_GET["page"] == $bid . ".html") {
	switch ($bfrom)
	{
		case "lq_list":
			header ("location: ./home.php?from=" . $bfrom . "&bid=" . $bid . "&title=" . $title . "&page=" . $bid . "_1/");
			exit();
			break;
		case "dd_list":
			header ("location: ./home.php?from=" . $bfrom . "&bid=" . $bid . "&title=" . $title . "&page=" . $bid . "-1.html");
			exit();
			break;		
		case "tt_list":
			header ("location: ./home.php?from=" . $bfrom . "&bid=" . $bid . "&title=" . $title . "&page=wapbook-" . $bid . "_1/");
			exit();
			break;		
		case "mf_list":
			header ("location: ./home.php?from=" . $bfrom . "&bid=" . $bid . "&title=" . $title . "&page=" . $bid . "_1/");
			exit();
			break;
	}
}

$page = $_GET["page"];

//分析提取小说内容
switch ($bfrom)
{
	case "lq_list":
		$shortid = mb_substr($bid, 0, 2);
		$url = "http://www.lequxs.com/du/{$shortid}/{$bid}/{$page}";
		if (!$article = mb_convert_encoding(getUrlData($url), "utf-8", "gbk")) {
			header ("location: ./error.php?fromurl=" . urlencode($_SERVER['PHP_SELF'] . '?' . $_SERVER['QUERY_STRING']));
		}

		$output = file_get_contents("./interface/article.interface");

		//获取小说正文
		preg_match('/<div id="BookText[\s\S]*?<\/div>/', $article, $contents);
		$contents = strip_tags($contents[0], "<br>");
		$contents = str_replace("&lt;script type=&quot;text/javascript&quot; src=&quot;/news/read/3.js&quot;&gt;&lt;/script&gt;", "", $contents);
		$contents = str_replace("&nbsp;&nbsp;&nbsp;&nbsp;", "　　", $contents);

		//获取当前章节名称
		preg_match('/<h1[\s\S]*?<\/h1>/', $article, $atitle);
		$atitle = strip_tags($atitle[0]);

		//获取上一章节链接
		preg_match("/<div class=\"link\"[\s\S]*?<\/div>/", $article, $ptitle);
		$ptitle = strip_tags($ptitle[0], "<a>");
		preg_match_all("/<a[\s\S]*?<\/a>/", $ptitle, $titles);
		$ptitle = $titles[0][1];

		//获取下一章节链接
		$ntitle = $titles[0][3];
		
		$output = str_replace("###TITLE###", $title, $output);
		$output = str_replace("###ARTICLETITLE###", $atitle, $output);
		$output = str_replace("###CONTENTS###", $contents, $output);
		$output = str_replace("###PREVTITLE###", $ptitle, $output);
		$output = str_replace("###NEXTTITLE###", $ntitle, $output);

		//$output = str_replace("###CATALOGLINK###", "./home.php?from={$bfrom}&bid={$bid}&title={$encodedtitle}&page=wapbook-{$bid}_1/", $output);
		//$output = str_replace("###CATALOG###", "<a id=\"pb_mulu\" href=\"./home.php?from={$bfrom}&bid={$bid}&title={$encodedtitle}&page=wapbook-{$bid}_1/\">目录</a>", $output);
		//$output = str_replace("/html/{$shortid}/{$bid}/", "./article.php?from={$bfrom}&bid={$bid}&title={$encodedtitle}&page=", $output);

		break;
	case "dd_list":
		$url = "http://m.dingdianzw.com/wapbook/{$bid}_{$page}"; 
		if (!$article = getUrlData($url)) {
			header ("location: ./error.php?fromurl=" . urlencode($_SERVER['PHP_SELF'] . '?' . $_SERVER['QUERY_STRING']));
		}

		$output = file_get_contents("./interface/article.interface");

		//获取小说正文
		preg_match('/<div id=\"chaptercontent[\s\S]*?<\/div>/', $article, $contents);
		//preg_match('/<br\/>[\s\S].*<br\/>/', $article, $contents);
		$contents = strip_tags($contents[0], "<br>");
		$contents = str_replace("&nbsp;&nbsp;&nbsp;&nbsp;", "　　", $contents);
		$contents = preg_replace('/<br\/>[\s\S]*?<br\/><br\/>/', "", $contents, 1);
		$contents = preg_replace('/<br\/><br\/>/', "", $contents, 1);

		//获取当前章节名称
		if (!$a = preg_match('/<span class=\"title\">[\s\S]*?<\/span>/', $article, $atitle)) {
			header ("location: ./home.php?from={$bfrom}&bid={$bid}&title={$encodedtitle}&page={$bid}-1.html");
			exit();
		}
		$atitle = strip_tags($atitle[0]);
		$atitle = explode("&nbsp;&nbsp;", $atitle);
		$atitle = $atitle[0];
		//$atitle = preg_replace("/\r\n/", "", $atitle);
		//$atitle = preg_replace("/\t/", "", $atitle);
		//$atitle = ltrim(rtrim($atitle));

		//获取上一章节链接
		preg_match("/<a href=[\s\S].*id=\"pt_prev\"[\s\S]*?<\/a>/", $article, $ptitle);
		$ptitle = $ptitle[0];

		//获取下一章节链接
		preg_match("/<a href=[\s\S].*id=\"pt_next\"[\s\S]*?<\/a>/", $article, $ntitle);
		$ntitle = $ntitle[0];
		
		$output = str_replace("###TITLE###", $title, $output);
		$output = str_replace("###ARTICLETITLE###", $atitle, $output);
		$output = str_replace("###CONTENTS###", $contents, $output);
		$output = str_replace("###PREVTITLE###", $ptitle, $output);
		$output = str_replace("###NEXTTITLE###", $ntitle, $output);

		$output = str_replace("###CATALOGLINK###", "./home.php?from={$bfrom}&bid={$bid}&title={$encodedtitle}&page={$bid}-1.html", $output);
		$output = str_replace("###CATALOG###", "<a id=\"pb_mulu\" href=\"./home.php?from={$bfrom}&bid={$bid}&title={$encodedtitle}&page={$bid}-1.html\">目录</a>", $output);
		$output = str_replace("/wapbook/{$bid}_", "./article.php?from={$bfrom}&bid={$bid}&title={$encodedtitle}&page=", $output);
		$output = str_replace("/wapbook/", "./article.php?from={$bfrom}&bid={$bid}&title={$encodedtitle}&page=", $output);
		$output = str_replace("『加入书签，方便阅读』" , "", $output);
		$output = str_replace("天才一秒记住本站地址:(顶点中文)m.dingdianzw.com 最快更新!无广告!" , "", $output);
		$output = str_replace(",点击下一页继续阅读。" , "", $output);
		$output = str_replace("_17mb_middle();" , "", $output);

		break;
	case "tt_list":
		$shortid = getShortId($bid);
		$url = "http://m.360118.com/html/{$shortid}/{$bid}/{$page}";
		if (!$article = mb_convert_encoding(getUrlData($url), "utf-8", "gbk")) {
			header ("location: ./error.php?fromurl=" . urlencode($_SERVER['PHP_SELF'] . '?' . $_SERVER['QUERY_STRING']));
		}
		
		$output = file_get_contents("./interface/article.interface");

		//获取小说正文
		preg_match('/<div id=\"nr1[\s\S]*?<\/div>/', $article, $contents);
		$contents = strip_tags($contents[0], "<br>");
		$contents = str_replace("&nbsp;&nbsp;&nbsp;&nbsp;", "　　", $contents);
		$contents = str_replace("（天天中文www.360118.com）", "", $contents);

		//获取当前章节名称
		preg_match('/<div class="nr_title[\s\S]*?<\/div>/', $article, $atitle);
		$atitle = strip_tags($atitle[0]);
		$atitle = preg_replace("/\r\n/", "", $atitle);
		$atitle = preg_replace("/\t/", "", $atitle);
		$atitle = ltrim(rtrim($atitle));

		//获取上一章节链接
		preg_match("/<a id=\"pb_prev[\s\S]*?<\/a>/", $article, $ptitle);
		$ptitle = $ptitle[0];

		//获取下一章节链接
		preg_match("/<a id=\"pb_next[\s\S]*?<\/a>/", $article, $ntitle);
		$ntitle = $ntitle[0];
		
		$output = str_replace("###TITLE###", $title, $output);
		$output = str_replace("###ARTICLETITLE###", $atitle, $output);
		$output = str_replace("###CONTENTS###", $contents, $output);
		$output = str_replace("###PREVTITLE###", $ptitle, $output);
		$output = str_replace("###NEXTTITLE###", $ntitle, $output);

		$output = str_replace("###CATALOGLINK###", "./home.php?from={$bfrom}&bid={$bid}&title={$encodedtitle}&page=wapbook-{$bid}_1/", $output);
		$output = str_replace("###CATALOG###", "<a id=\"pb_mulu\" href=\"./home.php?from={$bfrom}&bid={$bid}&title={$encodedtitle}&page=wapbook-{$bid}_1/\">目录</a>", $output);
		$output = str_replace("/html/{$shortid}/{$bid}/", "./article.php?from={$bfrom}&bid={$bid}&title={$encodedtitle}&page=", $output);

		break;
	case "mf_list":
		$shortid = getShortId($bid);
		$url = "http://m.freexs.cn/wapbook/{$shortid}_{$bid}_{$page}";
		if (!$article = mb_convert_encoding(getUrlData($url), "utf-8", "gbk")) {
			header ("location: ./error.php?fromurl=" . urlencode($_SERVER['PHP_SELF'] . '?' . $_SERVER['QUERY_STRING']));
		}
		
		$output = file_get_contents("./interface/article.interface");

		//获取小说正文
		preg_match('/<div id=\"nr1[\s\S]*?<\/div>/', $article, $contents);
		$contents = strip_tags($contents[0], "<br>");
		$contents = str_replace("&nbsp;&nbsp;&nbsp;&nbsp;", "　　", $contents);
		$contents = str_replace("bodytj()", "", $contents);

		//获取当前章节名称
		preg_match('/<div class="nr_title[\s\S]*?<\/div>/', $article, $atitle);
		$atitle = strip_tags($atitle[0]);
		//$atitle = preg_replace("/\r\n/", "", $atitle);
		//$atitle = preg_replace("/\t/", "", $atitle);
		//$atitle = ltrim(rtrim($atitle));

		//获取上一章节链接
		preg_match("/<a id=\"pb_prev[\s\S]*?<\/a>/", $article, $ptitle);
		$ptitle = $ptitle[0];

		//获取下一章节链接
		preg_match("/<a id=\"pb_next[\s\S]*?<\/a>/", $article, $ntitle);
		$ntitle = $ntitle[0];
		
		$output = str_replace("###TITLE###", $title, $output);
		$output = str_replace("###ARTICLETITLE###", $atitle, $output);
		$output = str_replace("###CONTENTS###", $contents, $output);
		$output = str_replace("###PREVTITLE###", $ptitle, $output);
		$output = str_replace("###NEXTTITLE###", $ntitle, $output);

		$output = str_replace("###CATALOGLINK###", "./home.php?from={$bfrom}&bid={$bid}&title={$encodedtitle}&page={$bid}_1/", $output);
		$output = str_replace("###CATALOG###", "<a id=\"pb_mulu\" href=\"./home.php?from={$bfrom}&bid={$bid}&title={$encodedtitle}&page=wapbook-{$bid}_1/\">目录</a>", $output);
		$output = str_replace("/wapbook/{$shortid}_{$bid}_", "./article.php?from={$bfrom}&bid={$bid}&title={$encodedtitle}&page=", $output);
		$output = str_replace("/wapbook/{$shortid}_", "./article.php?from={$bfrom}&bid={$bid}&title={$encodedtitle}&page=", $output);

		break;
	case "80_list":
}

echo $output;

$result = mysql_query("select * from book_history where bid='$bid' and uid='$uid' and bfrom='$bfrom' limit 1");

if (mysql_num_rows($result) > 0) {
	mysql_query("update book_history set btitle='$atitle', burl='./article.php?from=$bfrom&bid=$bid&page=$page&title=$title' where bid='$bid' and uid='$uid' and bfrom='$bfrom' limit 1");
	mysql_query("update book_list set last_read=CURRENT_TIMESTAMP where bid='$bid' and uid='$uid' and bfrom='$bfrom' limit 1");
} else {
	mysql_query("insert into book_history (bid, btitle, burl, bfrom, uid) values ('$bid', '$atitle', './article.php?from=$bfrom&bid=$bid&page=$page&title=$title', '$bfrom', '$uid')");
	mysql_query("update book_list set last_read=CURRENT_TIMESTAMP where bid='$bid' and uid='$uid' and bfrom='$bfrom' limit 1");
}

mysql_free_result($result);
mysql_close($conn);
?>