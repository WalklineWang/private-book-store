<?php
session_start();

if (!isset($_SESSION["uid"]) || !isset($_GET["fromurl"]) || empty($_GET["fromurl"])) {
	header ("location: ./");
	exit();
}

$fromUrl = $_GET["fromurl"];

$output = file_get_contents("./interface/error.interface");

$output = str_replace("###FROMURL###", $fromUrl, $output);

echo $output;
?>